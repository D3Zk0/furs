<?php

if (!defined('NICE_DATE_FORMAT')) {
    define('NICE_DATE_FORMAT', 'd. m. Y');
}


if (!defined('NICE_DATETIME_FORMAT')) {
    define('NICE_DATETIME_FORMAT', 'd. m. Y H:i');
}

if (!defined('ISO_DATE_FORMAT')) {
    define('ISO_DATE_FORMAT', 'Y-m-d');
}


if (!defined('ISO_FORMAT')) {
    define('ISO_FORMAT', 'Y-m-d H:i:s');
}




