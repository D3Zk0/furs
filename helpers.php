<?php
use Carbon\Carbon;

if (!function_exists('furs_path')) {
    function furs_path($path = ''): string
    {
        return __DIR__ . '/' . $path;
    }
}
if (!function_exists('cert_path')) {
    function cert_path($path = ''): string
    {
        return __DIR__ . '/certificates/' . $path;
    }
}


if (!function_exists('comma_format')) {
    function comma_format($number): string
    {
        return number_format($number, 2, ',', '');
    }
}

if (!function_exists('euro_format')) {
    function euro_format($number): string
    {
        return number_format($number, 2, ',', '') . ' €';
    }
}



if (!function_exists('df')) {
    /**
     * Format a given timestamp.
     *
     * @param string|\DateTimeInterface $timestamp
     * @param string $format
     * @return string
     */
    function df($timestamp, string $format = 'd. m. Y, H:i'): string
    {
        return Carbon::parse($timestamp)->format($format);
    }
}