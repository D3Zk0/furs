// vite.config.js in the subapp
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import path from 'path';
import fs from 'fs'
import { resolve } from 'path';
import { viteStaticCopy } from 'vite-plugin-static-copy'

const scssVariables = fs.readFileSync('src/resources/sass/imports/_variables.scss', 'utf-8')

function VueExtensionResolver() {
    return {
        name: 'vue-extension-resolver',
        async resolveId(source, importer) {
            if (importer && path.extname(source) === '') {
                const resolvedPath = path.resolve(path.dirname(importer), source);
                const vuePath = `${resolvedPath}.vue`;

                try {
                    await fs.promises.access(vuePath, fs.constants.F_OK);
                    return vuePath;
                } catch (error) {
                    // File doesn't exist, do nothing
                }
            }
        },
    };
}
export default defineConfig({
    plugins: [
        vue(),
        VueExtensionResolver(),
        viteStaticCopy({
            targets: [
                { src: 'dist/manifest.json', dest: '' },
            ]
        }),
    ],
    server: {
        port: 5174, // Set a specific port for the subapp
    },
    css: {
        preprocessorOptions: {
            scss: {
                additionalData: `${scssVariables}`
            }
        }
    },
    build: {
        outDir: 'dist', // Output directory for the build
        manifest: true,
        rollupOptions: {
            input: resolve(__dirname, 'src/resources/js/furs-app.js'),
            output: {
                entryFileNames: 'assets/[name].[hash].js',
                chunkFileNames: 'assets/[name].[hash].js',
                assetFileNames: 'assets/[name].[hash].[ext]',
            },
        },
    },
});
