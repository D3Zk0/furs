<?php

namespace wpm\furs\Utils;


use RuntimeException;

class Cert
{
    /**
     * Converts a PKCS#12 file (.p12) to a PEM file and saves it.
     * This function reads a .p12 file, extracts the certificate and private key,
     * and combines them into a single PEM file, saving it back to the same directory
     * with a .pem extension and returns the path of the new PEM file.
     *
     * @param string $path The file path to the .p12 file.
     * @param string $password The password for accessing the .p12 file.
     * @return string The path to the newly created PEM file.
     * @throws \RuntimeException If the .p12 file cannot be read or the password is incorrect.
     */
    public static function p12ToPemSave($path, $password): string
    {
        $content = file_get_contents($path);
        if ($content === false) {
            throw new RuntimeException("Unable to read file: " . $path);
        }

        $res = [];
        $openSSL = openssl_pkcs12_read($content, $res, $password);
        if (!$openSSL) {
            throw new RuntimeException("Error: " . openssl_error_string());
        }

        // Assemble the PEM content from the certificate and private key
        $cert = $res['cert'];
        $pkey = $res['pkey'];
        $extracerts = isset($res['extracerts']) ? implode('', $res['extracerts']) : '';
        $pemContent = $cert . $pkey . $extracerts;

        $pemPath = str_replace(".p12", ".pem", $path);  // Correctly replace .p12 with .pem for the new file's extension
        if (file_put_contents($pemPath, $pemContent) === false) {
            throw new RuntimeException("Failed to write PEM file: " . $pemPath);
        }

        return $pemPath;  // Return the path to the new PEM file
    }

    /**
     * Validates the password of a PKCS#12 certificate file.
     * This function attempts to read the .p12 file using the provided password.
     * If successful, it indicates the password is valid; otherwise, it will fail.
     *
     * @param string $certPath Path to the .p12 certificate file.
     * @param string $password Password to unlock the .p12 file.
     * @return bool Returns true if the password is correct, otherwise throws an exception.
     * @throws \RuntimeException If the certificate file does not exist or cannot be read.
     */
    public static function validateCertificatePassword($certPath, $password): bool
    {
        if (!$cert = file_get_contents($certPath)) {
            throw new RuntimeException("Certifikat " . $certPath . " ne obstaja!");
        }
        $cert_info = [];
        $isValid = openssl_pkcs12_read($cert, $cert_info, $password);
        if (!$isValid) {
            throw new RuntimeException("Napačno geslo ali ni mogoče prebrati certifikata.");
        }
        return true;
    }

}