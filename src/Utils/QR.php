<?php

namespace wpm\furs\Utils;

class QR
{

    /**
     * Converts a ZOI hexadecimal to decimal representation
     *
     * @param string $zoi The ZOI hexadecimal number.
     * @return string The decimal representation of the ZOI hexadecimal string as a string to preserve precision.
     */
    public static function getZOIdecimal($zoi)
    {
        $decimal = '0';
        $length = strlen($zoi);
        for ($i = 0; $i < $length; $i++) {
            $decimal = bcadd($decimal, bcmul(hexdec($zoi[$i]), bcpow(16, $length - $i - 1)));
        }
        return $decimal;
    }


}