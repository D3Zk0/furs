<?php

namespace wpm\furs\Commands;

use Illuminate\Console\Command;
use wpm\furs\Models\Invoice;

class CheckForFailedInvoiceReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'furs:report-failed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Executing this command checks for invoices that failed to be reported!';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Checking failed invoices...');
        $invoices = Invoice::failed()->whereNull("reported_at")->get()->each(fn ($invoice) => $invoice->report());
        $this->info('Failed invoicer reported again.');
    }
}
