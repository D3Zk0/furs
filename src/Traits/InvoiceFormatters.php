<?php

namespace wpm\furs\Traits;

use Carbon\Carbon;

trait InvoiceFormatters
{

    public function issueDateTime()
    {
        $tmp = new Carbon($this->created_at);
        return $tmp->format("Y-m-d") . "T" . $tmp->format("H:i:s");
    }


    public function taxesPerSeller()
    {
        return [["VAT" => $this->Taxes]];
    }


    public function invoiceIdentifier()
    {
        return [
            "BusinessPremiseID" => $this->premise->BusinessPremiseID,
            "ElectronicDeviceID" => $this->cash_register->number,
            "InvoiceNumber" => $this->InvoiceNumber . "",
        ];
    }


    /**
     * Generates tax details for a collection of items.
     *
     * This function takes a collection of items, each with a tax rate and price,
     * and returns a collection of tax details including the tax rate, taxable amount,
     * and tax amount, each rounded to 2 decimal places.
     *
     * @param \Illuminate\Support\Collection $items A collection of items. Each item should be an object
     *                                              or array with the following properties:
     *                                              - tax_rate (float): The tax rate for the item.
     *                                              - price (float): The full price of the item including tax.
     * @return \Illuminate\Support\Collection A collection of tax details, where each element is an array with:
     *                                         - 'TaxRate' (float): The tax rate, rounded to 2 decimals.
     *                                         - 'TaxableAmount' (float): The taxable amount, rounded to 2 decimals.
     *                                         - 'TaxAmount' (float): The tax amount, rounded to 2 decimals.
     *
     * @example
     * $items = collect([
     *     (object)['tax_rate' => 22.00, 'price' => 122.00],
     *     (object)['tax_rate' => 9.50, 'price' => 109.50],
     * ]);
     * $taxDetails = YourClass::generateTaxes($items);
     * // $taxDetails will be a collection of arrays with the tax details for each item.
     */
    public static function generateTaxes($items)
    {
        return $items->map(fn($item) => self::formatVAT($item->tax_rate, $item->price));
    }

    public static function formatVAT($TaxRate, $full_price)
    {
        $TaxRate = round($TaxRate, 2);
        $TaxAmount = self::cTaxAmount($full_price, $TaxRate);
        $TaxableAmount = self::cTaxableAmount($full_price, $TaxAmount);
        return compact('TaxRate', 'TaxableAmount', 'TaxAmount');
    }


    public static function cTaxAmount($full_price, $rate)
    {
        return round($full_price - $full_price / (1 + $rate / 100), 2);
    }

    public function cTaxableAmount($full_price, $taxAmount)
    {
        return round($full_price - $taxAmount, 2);
    }


    /**
     * Retrieves the next valid number for the current year by counting the number of records created in the current year
     * and incrementing by one. This function assumes it is part of an Eloquent model.
     *
     * @return int The next valid number for new records based on the current year's count.
     */
    public static function nextValidNumber()
    {
        $currentYear = date('Y');  // Get the current year
        return self::whereYear('created_at', $currentYear)->count() + 1;
    }

    public function reversedTaxes()
    {
        return collect($this->Taxes)->map(function ($tax) {
            return [
                "TaxRate" => $tax["TaxRate"],
                "TaxableAmount" => -$tax["TaxableAmount"],
                "TaxAmount" => -$tax["TaxAmount"]
            ];
        });
    }

    public function getReferenceInvoiceData()
    {
        if (!$this->invoice_id) {
            return null;
        }

        $related = self::find($this->invoice_id);
        return [[
            "ReferenceInvoiceIdentifier" => $related->invoiceIdentifier(),
            "ReferenceInvoiceIssueDateTime" => $related->issueDateTime(),
        ]];
    }


}