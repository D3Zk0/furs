<?php

namespace wpm\furs\Settings;

use Spatie\LaravelSettings\Settings;

class FursSetting extends Settings
{
    public $company_name;
    public $street;
    public $house_number;
    public $house_number_additional;

    public $community;
    public $city;

    public $zip;
    public $vat_number;
    public $register_number;
    public $phone_number;
    public $email;
    public $iban;
    public $logo;
    public $premise_configuration;
    public $mode;


    public static function group(): string
    {
        return 'furs';
    }
}
