<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

return new class extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('furs.company_name', '');
        $this->migrator->add('furs.street', '');
        $this->migrator->add('furs.house_number', '');
        $this->migrator->add('furs.house_number_additional', '');
        $this->migrator->add('furs.community', '');
        $this->migrator->add('furs.city', '');
        $this->migrator->add('furs.zip', '');
        $this->migrator->add('furs.vat_number', '');
        $this->migrator->add('furs.register_number', '');
        $this->migrator->add('furs.phone_number', '');
        $this->migrator->add('furs.email', '');
        $this->migrator->add('furs.logo', '');
        $this->migrator->add("furs.mode", "dev");
        $this->migrator->add("furs.premise_configuration", []);
        $this->migrator->add("furs.iban", '');
    }
};
