<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('failed_invoices', function (Blueprint $table) {
            $table->id();
            $table->string("type")->nullable();
            $table->foreignId("invoice_id")->nullable()->constrained()->onDelete("cascade");
            $table->string("reasons")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('failed_invoices');
    }
};
