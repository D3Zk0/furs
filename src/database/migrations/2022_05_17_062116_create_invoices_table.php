<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string("InvoiceNumber")->nullable();
            $table->string("full_invoice_number")->nullable();
            $table->float("InvoiceAmount")->nullable();
            $table->float("PaymentAmount")->nullable();
            $table->string("CustomerVATNumber")->nullable();
            $table->json("Taxes")->nullable();
            $table->binary("QR")->nullable();
            $table->string("EOR")->nullable();
            $table->string("ZOI")->nullable();
            $table->string("ProtectedID")->nullable();
            $table->string("OperatorTaxNumber")->nullable();
            $table->string("pdf")->nullable();
            $table->string("pdf_copy")->nullable();
            $table->string("reported_at")->nullable();
            $table->string("report_status")->nullable();
            $table->string("status")->default("draft");
            $table->foreignId("operator_id")->nullable()->constrained()->onDelete("cascade");
            $table->foreignId("invoice_id")->nullable()->constrained()->onDelete("cascade");
            $table->foreignId("business_unit_id")->constrained()->onDelete("cascade");
            $table->foreignId("premise_id")->constrained()->onDelete("cascade");
            $table->foreignId("cash_register_id")->constrained()->onDelete("cascade");
            $table->foreignId("customer_id")->nullable()->constrained()->onDelete("cascade");
            $table->json("customer")->nullable();
            $table->longText("stornation_reason")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
};
