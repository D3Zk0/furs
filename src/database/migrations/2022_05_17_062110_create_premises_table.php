<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('premises', function (Blueprint $table) {
            $table->id();
            $table->string("intern_name");
            $table->string("TaxNumber")->nullable();
            $table->string("BusinessPremiseID")->nullable();
            $table->string("CadastralNumber")->nullable();
            $table->string("BuildingNumber")->nullable();
            $table->string("BuildingSectionNumber")->nullable();
            $table->string("Street")->nullable();
            $table->string("HouseNumber")->nullable();
            $table->string("HouseNumberAdditional")->nullable();
            $table->string("Community")->nullable();
            $table->string("City")->nullable();
            $table->string("PostalCode")->nullable();
            $table->string("SpecialNotes")->nullable();
            $table->enum("PremiseType", ["A", "B", "C"])->nullable();
            $table->enum("cert_used", ["own", "unit"])->default("own");
            $table->string("cert_password")->nullable();
            $table->string("cert_path")->nullable();
            $table->string("cert_name")->nullable();
            $table->boolean("cert_verified")->default(false);
            $table->datetime("reported_at")->nullable();
            $table->boolean("closed")->default(false);
            $table->foreignId("business_unit_id")->constrained()->onDelete("cascade");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('premises');
    }
};
