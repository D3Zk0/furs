<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_units', function (Blueprint $table) {
            $table->id();
            $table->string("intern_name");
            $table->string("title");
            $table->string("TaxNumber")->nullable();
            $table->string("CadastralNumber")->nullable();
            $table->string("BuildingNumber")->nullable();
            $table->string("BuildingSectionNumber")->nullable();
            $table->string("Street")->nullable();
            $table->string("HouseNumber")->nullable();
            $table->string("HouseNumberAdditional")->nullable();
            $table->string("Community")->nullable();
            $table->string("City")->nullable();
            $table->string("PostalCode")->nullable();
            $table->string("cert_password")->nullable();
            $table->string("cert_path")->nullable();
            $table->string("cert_name")->nullable();
            $table->boolean("cert_verified")->nullable();
            $table->string("register_number")->nullable();
            $table->string("email")->nullable();
            $table->string("phone_number")->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_units');
    }
};
