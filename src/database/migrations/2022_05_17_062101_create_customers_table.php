<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string("name")->nullable();
            $table->string("surname")->nullable();
            $table->string("title")->nullable();
            $table->string("intern_name");
            $table->string("address")->nullable();
            $table->string("city")->nullable();
            $table->string("zip")->nullable();
            $table->string("country")->nullable();
            $table->string("description")->nullable();
            $table->string("tax_number")->unique();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
