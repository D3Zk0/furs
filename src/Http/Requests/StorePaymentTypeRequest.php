<?php

namespace wpm\furs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class StorePaymentTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $typeId = $this->get("id");

        return [
            'name' => 'required',
            'key' => [
                'required',
                'unique:payment_types,key,'.$typeId, // Assuming you meant 'key' instead of 'id' for the unique rule
                'regex:/^[a-z-]+$/' // Allow only lowercase letters and hyphens
            ],
            'active' => 'required|boolean',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "Vnos naziva je obvezen.",
            'key.required' => "Vnos ključa je obvezen.",
            'key.unique' => "Ključ je že v uporabi!",
            'key.regex' => "Ključ lahko vsebuje le male črke in znak '-'." // Custom message for regex validation
        ];
    }

    protected function prepareForValidation()
    {
        $this->replace($this->only([
            "id",
            "name",
            "key",
            "active",
        ]));
    }
}