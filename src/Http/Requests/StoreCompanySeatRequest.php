<?php

namespace wpm\furs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class StoreCompanySeatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "company_name" => "required|string",
            "street" => "required|string",
            "house_number" => "required|digits_between:1,10", // samo števke
            "house_number_additional" => "nullable|alpha", // samo črke, natanko 1 znak
            "community" => "required|string",
            "city" => "required|string",
            "zip" => "required|digits:4", // samo števke, natanko 4 znaki
            "vat_number" => "required|digits:8", // samo števke, natanko 8 znakov
            "register_number" => "required|digits:10", // natanko 10 znakov
            "email" => "nullable|email",
            "phone_number" => "nullable",
            "iban" => "required",
        ];
    }

    public function messages()
    {
        return [
            'company_name.required' => "Ime podjetja je obvezno.",
            'street.required' => "Ulica je obvezna.",
            'house_number.required' => "Hišna številka je obvezna.",
            'house_number.digits_between' => "Hišna številka mora vsebovati samo številke do 10 znakov.",
            'house_number_additional.alpha' => "Dodatna hišna številka mora vsebovati samo črke.",
            'community.required' => "Občina je obvezna.",
            'city.required' => "Mesto je obvezno.",
            'zip.required' => "Poštna številka je obvezna.",
            'zip.digits' => "Poštna številka mora vsebovati natanko 4 številke.",
            'vat_number.required' => "Davčna številka je obvezna.",
            'vat_number.digits' => "Davčna številka mora vsebovati natanko 8 številk.",
            'register_number.required' => "Matična številka je obvezna.",
            'register_number.digits' => "Matična številka mora vsebovati natanko 10 številk.",
            'email.email' => "Email mora biti veljaven e-poštni naslov.",
            'iban.required' => "Številka bančnega računa je obvezna!",
        ];
    }

    protected function prepareForValidation()
    {
        $this->replace($this->only([
            "company_name",
            "street",
            "house_number",
            "house_number_additional",
            "community",
            "city",
            "zip",
            "vat_number",
            "register_number",
            "email",
            "phone_number",
            "iban",
            "premise_configuration",
        ]));
    }
}