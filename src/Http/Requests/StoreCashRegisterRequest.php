<?php

namespace wpm\furs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class StoreCashRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "intern_name" => "required|string",
            "name" => "required|string",
            'number' => [
                'required',
                "string",
                'regex:/^[A-Za-z0-9]{1,20}$/'
            ],
            "business_unit_id" => "required|exists:business_units,id",
            "description" => "nullable",
        ];
    }

    public function messages()
    {
        return [
            'intern_name.required' => 'Interno ime je obvezno.',
            'name.required' => 'Ime naprave je obvezno.',
            'number.required' => 'Številka naprave je obvezna',
            'number.regex' => 'Številka naprave je ima lahko nabor znakov (A-Z,a-z,0-9) in mora biti dolga med 1 in 20 znaki.',
        ];
    }

    protected function prepareForValidation()
    {
        $this->replace($this->only([
            "id",
            "intern_name",
            "name",
            "number",
            "description",
            "business_unit_id",
        ]));
    }
}