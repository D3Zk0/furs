<?php

namespace wpm\furs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class StoreCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $customerId = $this->get("id");

        return [
            "intern_name" => "required|string",
            "title" => "nullable|string",
            "description" => "nullable|string",
            "name" => "required|string",
            "surname" => "required|string",
            "city" => "required|string",
            "zip" => "required|string",
            "address" => "required|string",
            'tax_number' => "required|string|unique:customers,tax_number,{$customerId}|between:1,20",
            "country" => "required|string",
        ];
    }

    public function messages()
    {
        return [
            "intern_name.required" => "Interno ime je obvezno.",
            "name.required" => "Ime je obvezno.",
            "surname.required" => "Priimek je obvezen.",
            "city.required" => "Mesto je obvezno.",
            "zip.required" => "Poštna številka je obvezna.",
            "address.required" => "Naslov je obvezen.",
            "tax_number.unique" => "Davčna številka že obstaja.",
            "tax_number.required" => "Davčna številka je obvezna.",
            "country.required" => "Država je obvezna.",
        ];
    }

    protected function prepareForValidation()
    {
        $this->replace($this->only([
            "id",
            "intern_name",
            "title",
            "description",
            "name",
            "surname",
            "city",
            "zip",
            "address",
            "tax_number",
            "country",
        ]));
    }
}