<?php

namespace wpm\furs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class ModeSwitchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'mode' => 'required|string|in:dev,prod',
        ];
    }

    public function messages()
    {
        return [
            'mode.required' => "Podatek za način delovanja je obvezen!",
            "mode.string" => "Podatek mora biti niz!",
            "mode.in" => "Način delovanja je lahko samo produkcija ali testiranje!"
        ];
    }

}