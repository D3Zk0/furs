<?php

namespace wpm\furs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class StorePremiseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'BusinessPremiseID' => 'required|string',
            'intern_name' => 'required|string',
            'PremiseType' => 'nullable|string|in:A,B,C',
            'TaxNumber' => 'required|digits:8',
        ];

        if (is_null($this->PremiseType)) {
            $rules = array_merge($rules, [
                'CadastralNumber' => 'required|digits_between:1,4',
                'BuildingNumber' => 'required|digits_between:1,5',
                'BuildingSectionNumber' => 'required|digits_between:1,4',
                'Street' => 'required|string',
                'HouseNumber' => 'required|digits_between:1,10',
                'HouseNumberAdditional' => 'nullable|alpha',
                'Community' => 'required|string',
                'City' => 'required|string',
                'PostalCode' => 'required|digits:4',
            ]);
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'intern_name.required' => 'Interno ime podjetja je obvezno.',
            'BusinessPremiseID.required' => 'Številka poslovnega prostora je obvezna.',
            'Street.required' => 'Ulica je obvezna.',
            'CadastralNumber.required' => 'Katastrska številka je obvezna.',
            'CadastralNumber.digits_between' => 'Katastrska številka morajo biti številke dolžine 1-4.',
            'BuildingNumber.required' => 'Številka stavbe je obvezna.',
            'BuildingNumber.digits_between' => 'Številka stavbe morajo biti številke dolžine 1-5.',
            'BuildingSectionNumber.required' => 'Številka dela stavbe je obvezna.',
            'BuildingSectionNumber.digits_between' => 'Številka dela stavbe morajo biti številke dolžine 1-4.',
            'HouseNumber.required' => 'Hišna številka je obvezna.',
            'HouseNumber.digits' => 'Hišna številka mora vsebovati samo številke.',
            'HouseNumberAdditional.alpha' => 'Dodatna hišna številka mora vsebovati samo črke.',
            'Community.required' => 'Občina je obvezna.',
            'City.required' => 'Mesto je obvezno.',
            'PostalCode.required' => 'Poštna številka je obvezna.',
            'PostalCode.digits' => 'Poštna številka mora vsebovati natanko 4 številke.',
            'TaxNumber.required' => 'Davčna številka je obvezna.',
            'TaxNumber.digits' => 'Davčna številka mora vsebovati natanko 8 številk.',
            'PremiseType.in' => 'Tip prostora mora biti A, B, C ali nepremični.',
        ];
    }

    protected function prepareForValidation()
    {
        $this->replace($this->only([
            'id',
            'intern_name',
            'BusinessPremiseID',
            'TaxNumber',
            'CadastralNumber',
            'BuildingNumber',
            'BuildingSectionNumber',
            'Street',
            'HouseNumber',
            'HouseNumberAdditional',
            'Community',
            'City',
            'PostalCode',
            'PremiseType',
            'SpecialNotes',
            'business_unit_id',
            'cert_verified',
            'cert_path',
            'cert_name',
            'cert_password',
            'cert_used',
        ]));
    }
}