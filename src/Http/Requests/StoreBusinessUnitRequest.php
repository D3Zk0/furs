<?php

namespace wpm\furs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class StoreBusinessUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "intern_name" => "required|string",
            "title" => "required|string",
            "TaxNumber" => "required|digits:8",
            "CadastralNumber" => "required|digits_between:1,4",
            "BuildingNumber" => "required|digits_between:1,5",
            "BuildingSectionNumber" => "required|digits_between:1,4",
            "Street" => "required|string",
            "HouseNumber" => "required|digits_between:1,10",
            "HouseNumberAdditional" => "nullable|alpha",
            "Community" => "required|string",
            "City" => "required|string",
            "PostalCode" => "required|digits:4",
            "register_number" => "required|digits:10",
            "email" => "nullable|email",
            "phone_number" => "nullable",
        ];
    }

    public function messages()
    {
        return [
            'intern_name.required' => "Interno ime podjetja je obvezno.",
            'title.required' => "Ime podjetja je obvezno.",
            'Street.required' => "Ulica je obvezna.",
            'CadastralNumber.required' => "Katastrska številka je obvezna.",
            'CadastralNumber.digits_between' => "Katastrska številka morajo biti številke dolžine 1-4",
            'BuildingNumber.required' => "Številka stavbe je obvezna.",
            'BuildingNumber.digits_between' => "Številka stavbe morajo biti številke dolžine 1-5",
            'BuildingSectionNumber.required' => "Številka dela stavbe je obvezna.",
            'BuildingSectionNumber.digits_between' => "Številka dela stavbe morajo biti številke dolžine 1-4",
            'HouseNumber.required' => "Hišna številka je obvezna.",
            'HouseNumber.digits' => "Hišna številka mora vsebovati samo številke.",
            'HouseNumberAdditional.alpha' => "Dodatna hišna številka mora vsebovati samo črke.",
            'Community.required' => "Občina je obvezna.",
            'City.required' => "Mesto je obvezno.",
            'PostalCode.required' => "Poštna številka je obvezna.",
            'PostalCode.digits' => "Poštna številka mora vsebovati natanko 4 številke.",
            'TaxNumber.required' => "Davčna številka je obvezna.",
            'TaxNumber.digits' => "Davčna številka mora vsebovati natanko 8 številk.",
            'register_number.required' => "Matična številka je obvezna.",
            'register_number.digits' => "Matična številka mora vsebovati natanko 10 številk.",
            'email.email' => "Email mora biti veljaven e-poštni naslov.",
        ];
    }

    protected function prepareForValidation()
    {
        $this->replace($this->only([
            "id",
            "intern_name",
            "title",
            "TaxNumber",
            "CadastralNumber",
            "BuildingNumber",
            "BuildingSectionNumber",
            "Street",
            "HouseNumber",
            "HouseNumberAdditional",
            "Community",
            "City",
            "PostalCode",
            "register_number",
            "email",
            "phone_number",
            "cert_verified",
            "cert_path",
            "cert_name",
            "cert_password",
        ]));
    }
}