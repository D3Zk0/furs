<?php

namespace wpm\furs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;


class StoreOperatorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'surname' => 'required|string',
            'vat_number' => ['required', 'numeric', 'digits:8', 'regex:/^[1-9][0-9]{7}$/'],
            'password' => 'sometimes|string|min:3',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "Vnos imena je obvezen.",
            'surname.required' => "Vnos priimka je obvezen.",
            'vat_number.required' => 'Vnos davčne številke je obvezen.',
            'vat_number.numeric' => 'Davčna številka mora biti številka.',
            'vat_number.digits' => 'Davčna številka mora biti dolga točno 8 števk.',
            'vat_number.regex' => 'Davčna številka se ne more začeti z 0.',
            'password.required' => 'Geslo je obvezno.',
            'password.string' => 'Geslo mora biti niz znakov.',
            'password.min' => 'Geslo mora vsebovati vsaj 3 znake.',
        ];
    }

    protected function prepareForValidation()
    {
        $this->replace($this->only([
            "id",
            "name",
            "surname",
            "vat_number",
            "active",
            "password",
            "business_unit_id",
        ]));
    }

}
