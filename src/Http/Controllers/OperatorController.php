<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use wpm\furs\Http\Requests\StoreOperatorRequest;
use wpm\furs\Models\Operator;

class OperatorController extends Controller
{

    public function paginate(Request $request)
    {
        try {
            $filter = json_decode(json_encode($request->filter));
            $query = Operator::query();

            $query->search(Operator::DEFAULT_SEARCH_COLS, $filter->search ?? null);

            return response()->success(SUCC::QUERY_MSG, $query->paginate($request->per_page));
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }


    public function show($id)
    {
        try {
            $operator = Operator::findOrFail($id);
            return response()->success(SUCC::QUERY_MSG, $operator);
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function store(StoreOperatorRequest $request)
    {
        try {

            DB::beginTransaction();
            $operator = Operator::store($request->all());
            DB::commit();

            return response()->success("Shranjevanje operatorja uspešno!", $operator);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $operator = Operator::findOrFail($id);
            $operator->delete();
            DB::commit();
            return response()->success(SUCC::DELETE_MSG, $operator);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }
}
