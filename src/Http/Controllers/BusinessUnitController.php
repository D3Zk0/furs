<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use wpm\furs\Http\Requests\StoreBusinessUnitRequest;
use wpm\furs\Models\BusinessUnit;

class BusinessUnitController extends Controller
{


    public function paginate(Request $request)
    {
        try {
            $filter = json_decode(json_encode($request->filter));
            $query = BusinessUnit::query();

            $query->search(BusinessUnit::DEFAULT_SEARCH_COLS, $filter->search ?? null);

            return response()->success(SUCC::QUERY_MSG, $query->paginate($request->per_page));
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function show( $id)
    {
        try {
            $businessUnit = BusinessUnit::findOrFail($id);
            return response()->success(SUCC::QUERY_MSG, $businessUnit);
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function store(StoreBusinessUnitRequest $request)
    {
        try {
            DB::beginTransaction();
            $businessUnit = BusinessUnit::store($request->all());
            DB::commit();
            return response()->success("Shranjevanje poslovne enote uspešno!", $businessUnit);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function delete($id)
    {
        try {
            $businessUnit = BusinessUnit::findOrFail($id);
            DB::beginTransaction();
            $businessUnit->delete();
            DB::commit();
            return response()->success("Poslovni prostor uspešno izbrisan!");
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }


}
