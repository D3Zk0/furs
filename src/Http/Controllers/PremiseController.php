<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use wpm\furs\API\fuCert;
use wpm\furs\Functions\Functions;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use wpm\furs\Http\Requests\StorePremiseRequest;
use wpm\furs\Models\Premise;

class PremiseController extends Controller
{


    public function paginate(Request $request)
    {
        try {
            $filter = json_decode(json_encode($request->filter));
            $query = Premise::query();

            if ($units = $filter->business_units ?? []) {
                $query->whereIn('business_unit_id', $units);
            }

            $query->search(Premise::DEFAULT_SEARCH_COLS, $filter->search ?? null);

            return response()->success(SUCC::QUERY_MSG, $query->paginate($request->per_page));
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function show($id)
    {
        try {
            $premise = Premise::findOrFail($id);
            return response()->success(SUCC::QUERY_MSG, $premise);
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function store(StorePremiseRequest $request)
    {
        try {
            DB::beginTransaction();
            $businessUnit = Premise::store($request->all());
            DB::commit();
            return response()->success("Shranjevanje poslovnega prostora uspešno!", $businessUnit);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $premise = Premise::findOrFail($id);
            $premise->delete();
            DB::commit();
            return response()->success(SUCC::DELETE_MSG, $premise);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    ###################################################################
    #                             CUSTOM                              #
    ###################################################################

    public function report($id)
    {
        try {
            DB::beginTransaction();
            $premise = Premise::findOrFail($id);
            $premise->report();
            DB::commit();
            return response()->success("Poslovni prostor uspešno poročan.");
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

}
