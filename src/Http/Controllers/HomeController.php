<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    //

    public function index(){

        return view("furs::app");
    }
}
