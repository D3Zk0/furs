<?php

namespace wpm\furs\Http\Controllers;

use Barryvdh\DomPDF\Facade\Pdf;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use wpm\furs\Models\Invoice;

class InvoiceController extends Controller
{




    public function paginate(Request $request)
    {
        try {
            $search = $request->search ?? "";
            $searchCols = $request->searchCols ?? Invoice::DEFAULT_SEARCH_COLS;
            $paginate = $request->paginate ?? Invoice::DEFAULT_PAGINATE;

            $query = Invoice::search($searchCols, $search);
            $query->with(["premise","business_unit"]);
            $query->orderBy("id", "desc");
            $results = $query->paginate($paginate);

            return response()->success(SUCC::QUERY_MSG, $results);
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function show(Invoice $invoice)
    {
        try {
            return response()->success(SUCC::QUERY_MSG, $invoice);
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $invoice = Invoice::create($data);
            DB::commit();

            return response()->success(SUCC::CREATE_MSG, $invoice);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }



    ###################################################################
    #                             CUSTOM                              #
    ###################################################################
    public function report(Invoice $invoice)
    {
        try {
            $invoice->report();
            return response()->success("Račun uspešno poročan!");
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function stornate(Invoice $invoice)
    {
        try {
            $invoice->stornate();
            return response()->success("Račun uspešno storniran!");
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function downloadInvoicePdf($id, Request $request)
    {
        $invoice = Invoice::find($id);
        if ($invoice) {
            $filePath = storage_path('app/' . $invoice->pdf);

            if (file_exists($filePath)) {
                return response()->file($filePath);
            }
        }
        return response()->json(['error' => 'Invoice not found'], 404);
    }

}
