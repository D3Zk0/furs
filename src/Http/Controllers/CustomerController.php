<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use wpm\furs\Http\Requests\StoreCustomerRequest;
use wpm\furs\Models\Customer;

class CustomerController extends Controller
{

    public function paginate(Request $request)
    {
        try {
            $filter = json_decode(json_encode($request->filter));
            $query = Customer::query();

            $query->search(Customer::DEFAULT_SEARCH_COLS, $filter->search ?? null);

            return response()->success(SUCC::QUERY_MSG, $query->paginate($request->per_page));
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }


    public function show($id)
    {
        try {
            $customer = Customer::findOrFail($id);
            return response()->success(SUCC::QUERY_MSG, $customer);
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function store(StoreCustomerRequest $request)
    {
        try {

            DB::beginTransaction();
            $customer = Customer::store($request->all());
            DB::commit();

            return response()->success("Shranjevanje stranke uspešno!", $customer);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $customer = Customer::findOrFail($id);
            $customer->delete();
            DB::commit();
            return response()->success(SUCC::DELETE_MSG, $customer);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }
}
