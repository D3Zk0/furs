<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use Illuminate\Http\Request;
use wpm\furs\Models\Invoice;


class FailedInvoiceController extends Controller
{
    //

    public static function logs($id)
    {
        $invoice = Invoice::findOrFail($id);

        $logs = $invoice->logs->toArray();
        $invoice_number = $invoice->full_invoice_number;

        dd(compact('invoice_number', "logs"));
    }


    public function paginate(Request $request)
    {
        try {
            $search = $request->search ?? "";
            $searchCols = $request->searchCols ?? Invoice::DEFAULT_SEARCH_COLS;
            $paginate = $request->paginate ?? Invoice::DEFAULT_PAGINATE;

            $query = Invoice::search($searchCols, $search);
            $query->whereHas("logs");
            $query->with(["premise","business_unit"]);
            $query->orderBy("id", "desc");
            $results = $query->paginate($paginate);

            return response()->success(SUCC::QUERY_MSG, $results);
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }



}
