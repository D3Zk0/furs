<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use wpm\furs\Http\Requests\ModeSwitchRequest;
use wpm\furs\Http\Requests\StoreCompanySeatRequest;
use wpm\furs\Settings\FursSetting;

class SettingsController extends Controller
{
    //

    public function getMode()
    {
        try {
            $settings = app(FursSetting::class);
            return response()->success(SUCC::QUERY_MSG, $settings->mode);
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function switchMode(ModeSwitchRequest $request)
    {
        try {
            DB::beginTransaction();
            $furs = app(FursSetting::class);
            $furs->mode = $request->mode;
            $furs->save();
            $message = $furs->mode == "prod" ? "PRODUKCIJA" : "TESTIRANJE";
            DB::commit();
            return response()->success("Davčna blagajna deluje v načinu: " . $message, $furs->mode);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function loadSettings()
    {
        try {
            $settings = app(FursSetting::class);
            return response()->success(SUCC::QUERY_MSG, $settings->toArray());
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }

    }

    public function storeInfo(StoreCompanySeatRequest $request)
    {
        try {
            DB::beginTransaction();
            $furs = app(FursSetting::class);
            $data = $request->all();
            foreach ($data as $key => $value) {
                $furs->{$key} = $value;
            }
            $furs->save();

            DB::commit();
            return response()->success("Podatki o sedežu podjetja uspešno shranjeni!");
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

}
