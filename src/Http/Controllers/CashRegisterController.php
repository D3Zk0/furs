<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use wpm\furs\Http\Requests\StoreCashRegisterRequest;
use wpm\furs\Models\CashRegister;

class CashRegisterController extends Controller
{
    //
    public function paginate(Request $request)
    {
        try {
            $filter = json_decode(json_encode($request->filter));
            $query = CashRegister::query();

            if($units = $filter->business_units ?? []){
                $query->whereIn('business_unit_id', $units);
            }

            $query->search(CashRegister::DEFAULT_SEARCH_COLS, $filter->search ?? null);

            return response()->success(SUCC::QUERY_MSG, $query->paginate($request->per_page));
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function show($id)
    {
        try {
            $register = CashRegister::findOrFail($id);
            return response()->success(SUCC::QUERY_MSG, $register);
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function store(StoreCashRegisterRequest $request){
        try {
            DB::beginTransaction();
            $register = CashRegister::store($request->all());
            DB::commit();
            return response()->success("Shranjevanje elektronske naprave uspešno.", $register);
        } catch (Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $register = CashRegister::findOrFail($id);
            $register->delete();
            DB::commit();
            return response()->success(SUCC::DELETE_MSG, $register);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }
}
