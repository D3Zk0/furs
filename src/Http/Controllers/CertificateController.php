<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Exception;
use wpm\furs\Utils\Cert;
use d3x\starter\Constants\ERR;

class CertificateController extends Controller
{
    //

    public function verify(Request $request)
    {
        try {
            // Initial validation to ensure file and password are present
            $request->validate([
                'file' => 'required|file', // Validate that the file is present and is a file
                'password' => 'required|string',
            ]);

            // Retrieve the file and password from the request
            $file = $request->file('file');
            $password = $request->input('password');

            // Manually check the MIME type
            $mimeType = $file->getClientMimeType();
            $allowedMimeTypes = ['application/x-pkcs12', 'application/octet-stream'];
            if (!in_array($mimeType, $allowedMimeTypes)) {
                throw new Exception("Invalid file type. Please upload a valid p12 or pfx file.");
            }

            // Define the storage path within the main application's storage directory
            $storagePath = storage_path('app/furs/certificates');

            // Create the directory if it doesn't exist
            if (!file_exists($storagePath)) {
                if (!mkdir($storagePath, 0755, true) && !is_dir($storagePath)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $storagePath));
                }
            }

            // Use the original filename and add a unique identifier only if necessary
            $originalFileName = $file->getClientOriginalName();
            $filePath = $storagePath . '/' . $originalFileName;

            if (file_exists($filePath)) {
                $fileNameWithoutExt = pathinfo($originalFileName, PATHINFO_FILENAME);
                $extension = $file->getClientOriginalExtension();
                $uniqueFileName = $fileNameWithoutExt . '_' . uniqid() . '.' . $extension;
                $filePath = $storagePath . '/' . $uniqueFileName;
            }

            // Move the uploaded file to the certificates directory
            $file->move($storagePath, basename($filePath));

            // Validate the certificate with the given password
            $valid = Cert::validateCertificatePassword($filePath, $password);

            // If the certificate is not valid, delete the file and throw an exception
            if (!$valid) {
                // Delete the file to avoid leaving invalid certificates on the server
                unlink($filePath);
                throw new Exception("Napačno geslo ali ni mogoče prebrati certifikata.");
            }

            Cert::p12ToPemSave($filePath, $password);

            // Return a success response with the file path, original name, and password
            return response()->json(['message' => 'Certifikat je veljaven!', 'file_path' => $filePath, 'original_file_name' => $originalFileName, 'password' => $password]);
        } catch (Exception $e) {
            // Return an error response in case of an exception
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }


    public function download(Request $request)
    {
        try {
            $filePath = $request->input('path');


            // Check if the file exists
            if (!file_exists($filePath)) {
                throw new \RuntimeException('File not found');
            }

            // Get the file's original name
            $fileName = basename($filePath);

            // Return a response with file download headers
            return response()->download($filePath, $fileName);

        } catch (Exception $e) {
            // Handle exceptions and return a JSON response
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }
}
