<?php

namespace wpm\furs\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use d3x\starter\Constants\ERR;
use d3x\starter\Constants\SUCC;
use wpm\furs\Http\Requests\StorePaymentTypeRequest;
use wpm\furs\Models\PaymentType;

class PaymentTypeController extends Controller
{

    public function paginate(Request $request)
    {
        try {
            $filter = json_decode(json_encode($request->filter));
            $query = PaymentType::query();

            $query->search(PaymentType::DEFAULT_SEARCH_COLS, $filter->search ?? null);

            return response()->success(SUCC::QUERY_MSG, $query->paginate($request->per_page));
        } catch (Exception $e) {
            return response()->error(ERR::BAD, $e->getMessage(), $e);
        }
    }


    public function show($id)
    {
        try {
            $payment_type = PaymentType::findOrFail($id);
            return response()->success(SUCC::QUERY_MSG, $payment_type);
        } catch (\Exception $e) {
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function store(StorePaymentTypeRequest $request)
    {
        try {

            DB::beginTransaction();
            $payment_type = PaymentType::store($request->all());
            DB::commit();

            return response()->success("Shranjevanje plačilne metode uspešno!", $payment_type);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }

    public function delete($id)
    {
        try {
            DB::beginTransaction();
            $payment_type = PaymentType::findOrFail($id);
            $payment_type->delete();
            DB::commit();
            return response()->success(SUCC::DELETE_MSG, $payment_type);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->error(ERR::BAD, ERR::MESSAGE, $e);
        }
    }
}
