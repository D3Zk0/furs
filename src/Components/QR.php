<?php

namespace wpm\furs\Components;

use Illuminate\View\Component;

class QR extends Component
{
    public $code;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        $this->code = $code;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('furs::pdf.components.qr');
    }
}