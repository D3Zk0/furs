<?php

namespace wpm\furs\Components;

use Illuminate\View\Component;
use wpm\furs\Settings\FursSetting;

class Identifiers extends Component
{
    public $eor;
    public $code;
    public $zoi;
    public $issuer;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($invoice)
    {
        $this->issuer = $invoice->operator ? $invoice->operator->full_name : app(FursSetting::class)->company_name;
        $this->code = $invoice->QR;
        $this->eor = $invoice->EOR;
        $this->zoi = $invoice->ZOI;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('furs::pdf.components.identifiers');
    }
}