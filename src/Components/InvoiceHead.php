<?php

namespace wpm\furs\Components;

use Illuminate\View\Component;
use wpm\furs\Settings\FursSetting;

class InvoiceHead extends Component
{
    public $settings;
    public $invoice;
    public $premise;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($invoice)
    {
        $this->settings = app(FursSetting::class);
        $this->invoice = $invoice;
        $this->premise = $invoice->premise;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('furs::pdf.components.invoice-head');
    }
}