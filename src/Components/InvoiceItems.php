<?php

namespace wpm\furs\Components;

use Illuminate\View\Component;

class InvoiceItems extends Component
{
    public $headings;
    public $rows;
    public $summary;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        if (isset($data["headings"]))
            $this->headings = $data["headings"];
        if (isset($data["rows"]))
            $this->rows = $data["rows"];
        if (isset($data["summary"]))
            $this->summary = $data["summary"];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('furs::pdf.components.items');
    }
}