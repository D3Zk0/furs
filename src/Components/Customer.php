<?php

namespace wpm\furs\Components;

use Illuminate\View\Component;

class Customer extends Component
{
    public $customer;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($invoice)
    {
        $this->customer = $invoice->customer;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('furs::pdf.components.customer');
    }
}