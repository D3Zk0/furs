<?php

return [
    "mode" => "dev",
    "software_supplier_vat" => env("FURS_SOFTWARE_SUPPLIER_TAX_NUMBER"),
];