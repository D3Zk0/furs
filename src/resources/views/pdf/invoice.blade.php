@extends('furs::pdf.layout')


@section('content')
    <div class="receipt-container">

        <x-invoice-head :invoice="$invoice" />

        <x-invoice-items :data="$items"/>

        <x-invoice-items :data="$ddv_items"/>

        <x-for-payment :value="$invoice->InvoiceAmount"/>

        <x-identifiers :invoice="$invoice"/>

        <h1 class="center marginTop" style="font-size: 1.5rem; margin-top: 20px">
            Hvala za obisk!
        </h1>

    </div>
@endsection


