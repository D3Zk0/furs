<table style="width: 100%">
    <tr>
        <td>
            <div class="left my5">
                <span class="fw-bold">{{ $settings->company_name }}</span><br>
                {{$settings->street}} {{$settings->house_number}}{{$settings->house_number_additional}} <br>
                {{ $settings->city}}, {{$settings->zip}}<br>
                ID za DDV: {{ $settings->vat_number }} <br>
                TRR: {{ $settings->iban }} <br>
            </div>
        </td>
    </tr>
</table>


<x-customer :invoice="$invoice"/>

<h1 class="left marginTop" style="font-size: 1.5rem;">
    RAČUN št.: {{$invoice->full_invoice_number}}
</h1>
<div class="left">
    @if($premise->City)
        {{$premise->City}},
    @endif
    {{ df($invoice->created_at, NICE_DATETIME_FORMAT)}}
</div>
<br>