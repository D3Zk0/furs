@if(isset($customer))
    <div class="fw-bold">{{$customer["full_name"]}}</div>
    <div>{{$customer["address"]}}</div>
    <div>{{$customer["city"]}}, {{$customer["zip"]}}</div>
    <div>{{$customer["country"]}}</div>
    <div>ID za DDV: {{$customer["tax_number"]}}</div>
    <br>
@endif
