<div class="left">Račun izdal: {{$issuer}}</div>
<br>

<div class="left"><span class="fw-bold">ZOI:</span> {{$zoi}}</div>

@if(isset($eor))
    <div class="left"><span class="fw-bold">EOR:</span> {{$eor}}</div>
    <x-qr :code="$code"/>
@else
    <div class="left"><span class="fw-bold">Ni povezave! </span>Račun bo davčno potrjen naknadno!</div>
@endif
