<table class="invoiceTable">
    <tr class="invoiceTableHead paddingTop">
        <td style="width: 50%; font-size: 1.5rem;">
            Za plačilo (EUR):
        </td>
        <td style="width: 40%; text-align: right; font-size: 1.5rem;">
            {{comma_format($value)}}
        </td>
    </tr>
</table>
<div class="dashedLine"></div>
