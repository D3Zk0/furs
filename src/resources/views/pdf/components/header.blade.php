@php
    $settings = app(\wpm\furs\Settings\FursSetting::class);
@endphp


@isset($logo)
    <div class="left">
        {{$logo}}
    </div>
@endisset

<table style="width: 100%">
    <tr>
        <td>
            <div class="left my5">
                <span class="fw-bold">{{ $settings->company_name }}</span><br>
                {{$settings->street}} {{$settings->house_number}}{{$settings->house_number_additional}} <br>
                {{ $settings->city}}, {{$settings->zip}}<br>
                ID za DDV: {{ $settings->vat_number }} <br>
                TRR: {{ $settings->iban }} <br>
            </div>
        </td>
    </tr>
</table>


@if(isset($customer))
    {{ $customer  }}
@endif

<h1 class="left marginTop" style="font-size: 1.5rem;">
    RAČUN št.: REC1-B5-2023
</h1>
<div class="left">Ljubljana, 28. 05. 2024, 16:40</div>
<br>