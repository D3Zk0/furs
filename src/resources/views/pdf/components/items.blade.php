<table class="invoiceTable dashedLine">
    <tr class="invoiceTableHead">
        @foreach ($headings as $heading)
            <td>{{ $heading['name'] }}</td>
        @endforeach
    </tr>
    @foreach ($rows as $row)
        <tr class="fs-1">
            @foreach ($headings as $heading)
                <td>{{ $row[$heading['key']] }}</td>
            @endforeach
        </tr>
    @endforeach
</table>
@if (!empty($summary))
    <div class="dashedLine"></div>
    <table class="invoiceTable summaryTable">
        @foreach ($summary as $item)
            <tr style="font-weight: bold">
                <td style="width: 50%; font-size: 1rem;">{{ $item['label'] }}</td>
                <td style="text-align: right;">{{ $item['value'] }}</td>
            </tr>
        @endforeach
    </table>
@endif