<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&amp;display=swap" rel="stylesheet">
    <style>
        * {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
            font-family: "Open Sans", sans-serif;
            font-size: 9px;
        }
        .fw-bold{
            font-weight: bold;
        }

        .dashedLine {
            border-top-style: solid;
            border-bottom-style: none;
            padding: 4px 0;
            border-color: black;
            border-width: 1px;
        }

        .dashedLineTD {
            border: 1px dashed black;
        }

        .left {
            text-align: left;
        }

        .right {
            text-align: right;
        }

        .center {
            text-align: center;
        }

        img {
            width: 18mm;
            height: 12mm;
        }

        #QR {
            width: 20mm;
            height: 20mm;
            margin: 10px auto 0;
        }

        .d-none {
            display: none;
        }

        .pagebreak {
            clear: both;
            page-break-after: always;
        }

        .invoiceTable {
            width: 100%;
            /* margin: 5px; */
        }

        .invoiceTableHead {
            font-weight: bold;
            padding: 7px 0 7px 0;
        }

        .invoiceTableRow {
            width: 100%;
        }

        .paddingTop {
            padding-top: 30px;
        }

        .l10 {
            width: 17%;
            text-align: right;
        }

        .l20 {
            width: 17%;
        }

        .l30 {
            width: 32%;
        }

        /*.marginTop {*/
        /*    margin-top: 15px;*/
        /*}*/
        .my5 {
            margin-bottom: 5px;
            margin-top: 5px
        }

        .mb-10 {
            margin-bottom: 10px;
        }

        td:last-child {
            text-align: right;
        }

        .receipt-container {
            margin-left: 0.25cm;
            margin-right: 0.25cm;
        }

        .fs-1{
            font-size: 1rem;
        }

    </style>
    <title>Invoice</title>
</head>
<body>
@yield('content')
</body>
</html>


