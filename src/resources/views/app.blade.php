<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Davčna blagajna</title>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet">
    <script src="https://wpm-app.com/countries.js"></script>
    @if (app()->environment('local'))
        <script type="module" src="http://localhost:5174/src/resources/js/furs-app.js"></script>
    @else
        @php
            $manifest = json_decode(file_get_contents(public_path('furs-app/.vite/manifest.json')), true);
            $jsFile = $manifest['src/resources/js/furs-app.js']['file'];
      $cssFile = $manifest['src/resources/js/furs-app.js']['css'][0];
      @endphp
    <link rel="stylesheet" href="{{ asset('furs-app/' . $cssFile) }}">
        <script type="module" src="{{ asset('furs-app/' . $jsFile) }}"></script>
    @endif

</head>
<body>
<div id="app">
</div>
</body>
</html>
