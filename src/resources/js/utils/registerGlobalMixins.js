import routing from "../mixins/routing.js";

export default function registerGlobalMixins(app){
    // app.mixin(translate);
    app.mixin(routing);

}
