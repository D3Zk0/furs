import {
    Input,
    Loader,
    Scroll,
    Button,
    Card,
    Layout,
    Table,
    TopTabs,
    Toggle,
    ModalWindow, Section
} from "wpm-starter";


import vSelect from 'vue-select'
import "vue-select/dist/vue-select.css";
export default function registerGlobalComponents(app) {
    //CLEAN
    app.component('Input', Input);
    app.component('Section', Section);
    app.component('v-select', vSelect);
    app.component('Loader', Loader);
    app.component('Toggle', Toggle);
    app.component('Button', Button);
    app.component('Card', Card);
    app.component('Layout', Layout);
    app.component('Table', Table);
    app.component('Scroll', Scroll);
    app.component('TopTabs', TopTabs);
    app.component('Modal', ModalWindow);
}
