import "./bootstrap.js"
import {createApp} from 'vue';
import App from './App.vue';

import '../sass/app.scss';
import router from "./routes/router.js";
import registerGlobalComponents from "./utils/registerGlobalComponents.js";
import registerGlobalPlugins from "./utils/registerGlobalPlugins.js";
import registerGlobalMixins from "./utils/registerGlobalMixins.js";

let app =
    createApp(App)
        .use(router)

registerGlobalComponents(app);
registerGlobalMixins(app);
registerGlobalPlugins(app);

app.mount("#app");