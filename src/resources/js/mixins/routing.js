export default {
    methods: {
        _openAdminRoute(item, path = null, query = null) {
            path = path ? path : this.$route.path;
            query = query? query: this.$route.query;
            return {
                path: path,
                query: {
                    ...query,
                    open: item ? item.id : null,
                }
            }
        },
        _euro_format(value) {
            let n = typeof value == "number" ? parseFloat(value) : 0;
            return Number(n).toLocaleString('si', {minimumFractionDigits: 2, maximumFractionDigits: 2})+"€";
        },
    }
}