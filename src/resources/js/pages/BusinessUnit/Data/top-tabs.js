import home_sections from "../../Home/Data/home_sections.js";

export default {
    data() {
        return {
            require_id_tabs: [],
            section: null // ime sekcije, obvezno definiraj v page komponenti
        }
    },
    computed: {
        active() {
            if (!this.$route.query.tab) {
                this.$router.replace({
                    query: {
                        ...this.$route.query,
                        tab: this.tabFormat[0].tab_key
                    }
                });
            }
            return this.$route.query.tab;
        },
        tabFormat() {
            let section = home_sections.find(a => a.title == this.section);
            if (!section) return [];

            let children = [...section.child];

            if (this.lockedTabs && this.lockedTabs.length > 0) {
                children = children.filter(child => !this.lockedTabs.includes(child.tab));
            }
            children = children.filter(child => !child.home_only);

            return children.map(child => ({
                tab_key: child.tab,
                tab_label: child.title
            }));

        },
        lockedTabs() {
            if (!this.$route.query.open) {
                return this.require_id_tabs;
            }
            return [];
        },

    }
}