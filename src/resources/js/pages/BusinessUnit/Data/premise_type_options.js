export default{
    data(){
        return {
            type_options: [
                {
                    label: "A - premičen objekt (npr. prevozno sredstvo, premična stojnica)",
                    value: "A"
                },
                {
                    label: "B – objekt na stalni lokaciji",
                    value: "B"
                },
                {
                    label: "C – posamezna elektronska naprava za izdajo računov ali vezana knjiga računov v primerih, ko zavezanec ne uporablja drugega poslovnega prostora.",
                    value: "C"
                },
                {
                    label: "Nepremični",
                    value: null
                },
            ]
        }
    }
}