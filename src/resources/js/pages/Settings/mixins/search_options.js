import { debounce } from "wpm-starter";
import axios from 'axios';

export default {
    data() {
        return {
            loading: false,
            debouncedSearchPremises: debounce(this.createSearchHandler('/api/premise/paginate', 'premises', 'business_units'), 300),
            debouncedSearchUnits: debounce(this.createSearchHandler('/api/business-unit/paginate', 'business_units'), 300),
            debouncedSearchRegisters: debounce(this.createSearchHandler('/api/register/paginate', 'registers', 'business_units'), 300),
            totalPages: null,
            searchQuery: null,
            registers: [],
            premises: [],
            business_units: [],
            configuration: null,
        }
    },
    methods: {
        createSearchHandler(endpoint, property, dependency = null) {
            return (search, loading, force) => {
                if (!search && !force) return;

                if (dependency && !this.premiseConfiguration[this.configuration_key].business_unit) {
                    // If dependency is required but not selected, do not perform the search
                    loading(false);
                    return;
                }

                if (this.searchQuery !== search) {
                    this.currentPage = 1;
                    this[property] = [];
                }
                this.searchQuery = search;
                loading(true);
                this.loading = true;

                const filter = { search: search };

                if (dependency) {
                    filter[dependency] = [this.premiseConfiguration[this.configuration_key].business_unit.id]
                }

                axios.post(endpoint, { filter: filter, page: this.currentPage })
                    .then(({ data }) => {
                        this[property] = this.currentPage === 1 ? data.data.data : this[property].concat(data.data.data);
                        this.totalPages = data.data.last_page;
                        this.currentPage++;
                    })
                    .catch(error => {
                        this._handleErrorNotification(error);
                    })
                    .finally(() => {
                        loading(false);
                        this.loading = false;
                    });
            };
        },
        attachScrollListener(key) {
            this.configuration_key = key;
            this.$nextTick(() => {
                const dropdown = this.$el.querySelector('.vs__dropdown-menu');
                if (dropdown) {
                    dropdown.addEventListener('scroll', this.handleScroll, { passive: true });
                }
            });
        },
        handleScroll(event) {
            const { target } = event;
            if (target.scrollHeight - target.scrollTop === target.clientHeight) {
                if (this.currentPage <= this.totalPages) {
                    this.createSearchHandler(this.searchQuery, () => { }, true);
                }
            }
        },
    },
    beforeDestroy() {
        const dropdown = this.$el.querySelector('.vs__dropdown-menu');
        if (dropdown) {
            dropdown.removeEventListener('scroll', this.handleScroll);
        }
    },

}
