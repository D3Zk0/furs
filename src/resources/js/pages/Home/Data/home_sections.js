export default [
    {
        title: "Osnovno",
        icon: "fa fa-list",
        href: "/",
        child: [
            {
                title: "Računi",
                icon: "fa fa-book",
                show_on_home:true,
                get href() {
                    return `/racuni`;
                }
            },
            {
                title: "Stranke",
                icon: "fa fa-users",
                show_on_home:true,
                get href() {
                    return `/stranke`;
                }
            },
            {
                title: "Čakalna vrsta",
                icon: "fa fa-clock",
                show_on_home:true,
                get href() {
                    return `/cakalna-vrsta`;
                }
            },
            {
                title: "Nastavitve",
                icon: "fa fa-cogs",
                show_on_home:true,
                get href() {
                    return `/nastavitve`;
                }
            },
            {
                title: "Poslovne enote",
                icon: "fa fa-list",
                show_on_home:true,
                get href() {
                    return `/poslovne-enote`;
                }
            },

        ],

    },

]
