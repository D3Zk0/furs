import {createRouter, createWebHistory} from 'vue-router';
//PAGES IMPORT
import {applyMiddleware} from "wpm-starter";
import invoice_routes from "./invoice_routes.js";
import setting_routes from "./setting_routes.js";
import HomePage from "../pages/Home/HomePage.vue";
import queue_routes from "./queue_routes.js";
import unit_routes from "./unit_routes.js";
import customer_routes from "./customer_routes.js";


const routes = [
    {
        path: '',
        name: "Domov",
        component: HomePage,
    },
    ...invoice_routes,
    ...unit_routes,
    ...customer_routes,
    ...queue_routes,
    ...setting_routes,
];

const router = createRouter({
    history: createWebHistory('/furs'), // Set the base path here
    routes
});

//APPLYING MIDDLEWARE LOGIC FOR ROUTER
applyMiddleware(router);

export default router;
