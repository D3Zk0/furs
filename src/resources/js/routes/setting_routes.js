import SettingsPage from "../pages/Settings/SettingsPage.vue";


export default [
    {
        path: '/nastavitve',
        name: "Nastavitve",
        component: SettingsPage,
    },
]