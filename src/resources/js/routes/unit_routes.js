import BusinessUnitsPage from "../pages/BusinessUnit/BusinessUnitsPage.vue";
import EditBusinessUnitPage from "../pages/BusinessUnit/EditBusinessUnitPage.vue";


export default [
    {
        path: '/poslovne-enote',
        name: "poslovne-enote",
        children: [
            {
                path: "",
                name: "Poslovne enote",
                component: BusinessUnitsPage
            },
            {
                path: "uredi/:id?",
                name: "Dodaj poslovno enoto",
                component: EditBusinessUnitPage
            }
        ]
    }

]