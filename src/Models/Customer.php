<?php

namespace wpm\furs\Models;

use d3x\starter\Traits\ModelHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use wpm\furs\Traits\PublishesMigrations;

class Customer extends Model
{
    use HasFactory, PublishesMigrations, ModelHelpers, SoftDeletes;

    ###################################################################
    #                        INITIALIZATION                           #
    ###################################################################
    protected $fillable = [
        "name",
        "surname",
        "intern_name",
        "title",
        "address",
        "city",
        "zip",
        "country",
        "tax_number",
        "description"
    ];
    protected $with = [];
    protected $appends = ["full_name"];

    public const DEFAULT_SEARCH_COLS = [
        "id",
        "title",
        "description",
        "name",
        "surname",
        "intern_name",
        "address",
        "city",
        "zip",
        "country",
        "tax_number"
    ];

    public const DEFAULT_PAGINATE = 30;

    ###################################################################
    #                           RELATIONS                             #
    ###################################################################
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    ###################################################################
    #                           ACCESSORS                             #
    ###################################################################
    public function getFullNameAttribute()
    {
        $fn = "";
        if ($this->title)
            $fn .= $this->title . " - ";
        $fn .= $this->surname . " " . $this->name;
        return $fn;
    }
    ###################################################################
    #                           MUTATORS                              #
    ###################################################################

    ###################################################################
    #                             SCOPES                              #
    ###################################################################

    ###################################################################
    #                           FUNCTIONS                             #
    ###################################################################

}
