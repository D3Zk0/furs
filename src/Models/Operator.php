<?php

namespace wpm\furs\Models;

use d3x\starter\Traits\ModelHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;
use wpm\furs\Traits\PublishesMigrations;

class Operator extends Model
{
    use  PublishesMigrations, ModelHelpers, SoftDeletes;

    ###################################################################
    #                        INITIALIZATION                           #
    ###################################################################
    protected $fillable = [
        "name",
        "surname",
        "vat_number",
        "password",
        "active",
        "business_unit_id",
    ];
    protected $with = [];
    protected $appends = [
        "full_name"
    ];

    protected $hidden = ["password"];
    protected $casts = [
        "password" => "hashed",
        "active" => "boolean",
    ];


    public const DEFAULT_SEARCH_COLS = [
        "id",
        "name",
        "surname",
        "vat_number"
    ];


    public const DEFAULT_PAGINATE = 30;

    ###################################################################
    #                           RELATIONS                             #
    ###################################################################
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    ###################################################################
    #                           ACCESSORS                             #
    ###################################################################
    public function getFullNameAttribute()
    {
        return $this->surname . " " . $this->name;
    }
    ###################################################################
    #                           MUTATORS                              #
    ###################################################################
    public function setPasswordAttribute($password)
    {
        $password = $password ?: "qwe";
        $this->attributes["password"] = Hash::make($password);
    }

    ###################################################################
    #                             SCOPES                              #
    ###################################################################
    public function scopeActive($query)
    {
        return $query->where("active", true);
    }

    ###################################################################
    #                           FUNCTIONS                             #
    ###################################################################

}
