<?php

namespace wpm\furs\Models;

use d3x\starter\Traits\ModelHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use wpm\furs\Traits\PublishesMigrations;

class PaymentType extends Model
{
    use HasFactory, PublishesMigrations, ModelHelpers, SoftDeletes;

    ###################################################################
    #                        INITIALIZATION                           #
    ###################################################################
    protected $fillable = [
        "name",
        "key",
        "active",
    ];

    protected $casts = [
        "active" => "boolean",
    ];

    public $timestamps = false;
    protected $with = [];
    protected $appends = [];

    public const DEFAULT_SEARCH_COLS = [
        "id",
        "name",
        "key",
    ];
    public const DEFAULT_PAGINATE = 30;

    ###################################################################
    #                           RELATIONS                             #
    ###################################################################
    public function values()
    {
        return $this->hasMany(InvoicePayment::class);
    }


    ###################################################################
    #                           ACCESSORS                             #
    ###################################################################

    ###################################################################
    #                           MUTATORS                              #
    ###################################################################

    ###################################################################
    #                             SCOPES                              #
    ###################################################################
    public function scopeActive($query)
    {
        return $query->where("active", true);
    }

    ###################################################################
    #                           FUNCTIONS                             #
    ###################################################################

    public function mapSumValues($invoices_ids = null)
    {
        if (!$invoices_ids)
            return [
                "id" => $this->id,
                "name" => $this->name,
                "key" => $this->key,
                "sum_value" => $this->values->sum("value"),
            ];

        return [
            "id" => $this->id,
            "key" => $this->key,
            "name" => $this->name,
            "sum_value" => $this->values()->whereIn("invoice_id", $invoices_ids)->sum("value")
        ];

    }

}
