<?php

namespace wpm\furs\Models;

use App\Services\InvoiceGenerator;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use wpm\furs\API\FURS;
use wpm\furs\Traits\InvoiceFormatters;
use wpm\furs\Traits\PublishesMigrations;

class Invoice extends Model
{
    use HasFactory, PublishesMigrations, InvoiceFormatters;

    ###################################################################
    #                        INITIALIZATION                           #
    ###################################################################
    protected $fillable = [
        "InvoiceNumber",
        "full_invoice_number",
        "InvoiceAmount",
        "PaymentAmount",
        "CustomerVATNumber",
        "Taxes",
        "QR",
        "EOR",
        "ZOI",
        "ProtectedID",
        "OperatorTaxNumber",
        "pdf",
        "pdf_copy",
        "reported_at",
        "report_status",
        "status",
        "operator_id",
        "invoice_id",
        "business_unit_id",
        "cash_register_id",
        "premise_id",
        "customer_id",
        "customer",
        "stornation_reason",
    ];

    protected $with = [];
    protected $appends = [];

    protected $casts = [
        "customer" => "array",
        "Taxes" => "array",
        "InvoiceAmount" => "float",
    ];
    public const DEFAULT_SEARCH_COLS = [
        "InvoiceNumber",
        "customer",
        "InvoiceAmount"
    ];
    public const DEFAULT_PAGINATE = 30;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($invoice) {
            $invoice->setCustomerVATNumber();
            $invoice->setOperatorTaxNumber();
            $invoice->setInvoiceNumberAndFullInvoiceNumber();
            $invoice->setZOIandProtectedID();
        });

        static::updating(function ($invoice) {
            $invoice->setCustomerVATNumber();
            $invoice->setOperatorTaxNumber();
        });
    }

    public function setCustomerVATNumber()
    {
        if (isset($this->customer) && isset($this->customer['tax_number'])) {
            $this->CustomerVATNumber = $this->customer['tax_number'];
        }
    }

    public function setOperatorTaxNumber()
    {
        if ($this->operator_id) {
            $operator = $this->operator;
            if ($operator && isset($operator->vat_number)) {
                $this->OperatorTaxNumber = $operator->vat_number;
            }
        }
    }

    public function setInvoiceNumberAndFullInvoiceNumber()
    {
        $businessPremiseID = $this->premise->BusinessPremiseID;
        $cashRegisterNumber = $this->cash_register->number;

        // Fetch the last invoice for this premise and cash register
        $lastInvoice = self::where('premise_id', $this->premise_id)
            ->where('cash_register_id', $this->cash_register_id)
            ->orderBy('id', 'desc')

            ->lockForUpdate()
            ->first();
        $lastNumber = $lastInvoice ? intval($lastInvoice->InvoiceNumber) : 0;
        $this->InvoiceNumber = $lastNumber + 1;
        $this->full_invoice_number = sprintf('%s-%s-%d', $businessPremiseID, $cashRegisterNumber, $this->InvoiceNumber);
    }

    public function setZOIandProtectedID()
    {
        $businessPremiseID = $this->premise->BusinessPremiseID;
        $electronicDeviceID = $this->cash_register->number;
        $cert = $this->premise->getCertificate();

        $newIssueDateTime = date("d.m.Y H:i:s", strtotime($this->issueDateTime()));
        $signData = $cert->getTaxNumber() . $newIssueDateTime . $businessPremiseID . $electronicDeviceID . $this->InvoiceNumber;

        // create signature
        $key = $cert->getKey();

        openssl_sign($signData, $signature, $key, OPENSSL_ALGO_SHA256);
        #  openssl_free_key($key);

        $this->ZOI = md5($signature);
        $this->ProtectedID = $this->ZOI;
    }



    ###################################################################
    #                           RELATIONS                             #
    ###################################################################
    public function cash_register()
    {
        return $this->belongsTo(CashRegister::class);
    }

    public function premise()
    {
        return $this->belongsTo(Premise::class);
    }

    public function business_unit()
    {
        return $this->belongsTo(BusinessUnit::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }


    public function operator()
    {
        return $this->belongsTo(Operator::class);
    }

    public function logs()
    {
        return $this->hasMany(FailedInvoice::class);
    }

    public function stornated(){
        return $this->belongsTo(self::class,"invoice_id");
    }

    public function storno(){
        return $this->hasOne(self::class,"invoice_id");
    }


    ###################################################################
    #                           ACCESSORS                             #
    ###################################################################

    ###################################################################
    #                           MUTATORS                              #
    ###################################################################
    public function setCustomerAttribute($value)
    {
        $this->attributes["customer"] = json_encode($value);
    }

    public function setTaxesAttribute($value)
    {
        $this->attributes["Taxes"] = json_encode($value);
    }
    ###################################################################
    #                             SCOPES                              #
    ###################################################################
    public function scopeSuccessful($query)
    {
        return $query->where("report_status", "successful");
    }

    public function scopeFailed($query)
    {
        return $query->where("report_status", "failed");
    }

    public function scopeStornated($query)
    {
        return $query->where("status", "stornated");
    }

    public function scopeValid($query)
    {
        return $query->where("status", "valid");
    }

    public function scopeStorni($query)
    {
        return $query->where("status", "storno");
    }

    ###################################################################
    #                           FUNCTIONS                             #
    ###################################################################
    public function stornate()
    {
        // Duplicate the original invoice
        $invoice = $this->replicate();

        // Negate the amounts
        $invoice->InvoiceAmount = -$this->InvoiceAmount;
        $invoice->PaymentAmount = -$this->PaymentAmount;
        $invoice->Taxes = $this->reversedTaxes();

        // Reset specific attributes
        $attributesToReset = [
            'QR',
            'id',
            'EOR',
            'pdf',
            'pdf_copy',
            "ZOI",
            "ProtectedID",
            'reported_at',
            'report_status',
        ];

        foreach ($attributesToReset as $attribute) {
            $invoice->$attribute = null;
        }

        // Set the status and invoice_id
        $invoice->status = "storno";
        $invoice->invoice_id = $this->id;

        // Save the new invoice
        $invoice->save();
        $this->update(["status" => "stornated"]);
        return $invoice;
    }


    public function report()
    {
        if (!$this->premise)
            throw new Exception("Poslovni prostor ni definiran!");

        try {
            $furs = new FURS($this->premise->getCertificate());
            $furs->fuInvoice($this);
        } catch (Exception $exception) {
            $this->update([
                "report_status" => "failed",
                "reported_at" => null,
            ]);

            FailedInvoice::create([
                'type' => $exception->getCode(),
                'invoice_id' => $this->id,
                'reasons' => $exception->getMessage(),
            ]);
        }
        if (!in_array($this->status, ["storno", "stornated"]))
            $this->update(["status" => "valid"]);
        $this->preparePDF();
    }


    public function storeAsFailed()
    {
    }


    public function preparePDF()
    {
    }
}
