<?php

namespace wpm\furs\Models;

use d3x\starter\Traits\ModelHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use wpm\furs\Traits\PublishesMigrations;

class FailedInvoice extends Model
{
    use HasFactory, PublishesMigrations, ModelHelpers;

    ###################################################################
    #                        INITIALIZATION                           #
    ###################################################################
    protected $fillable = [
        "id",
        "type",
        "invoice_id",
        "reasons",
    ];
    protected $with = [];

    public const DEFAULT_SEARCH_COLS = [
        "id",
        "type",
        "invoice_id",
        "reasons",
    ];

    public const DEFAULT_PAGINATE = 30;

    ###################################################################
    #                           RELATIONS                             #
    ###################################################################
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    ###################################################################
    #                           ACCESSORS                             #
    ###################################################################

    ###################################################################
    #                           MUTATORS                              #
    ###################################################################

    ###################################################################
    #                             SCOPES                              #
    ###################################################################

    ###################################################################
    #                           FUNCTIONS                             #
    ###################################################################

}
