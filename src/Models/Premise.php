<?php

namespace wpm\furs\Models;

use Carbon\Carbon;
use d3x\starter\Traits\ModelHelpers;
use http\Exception\RuntimeException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use wpm\furs\API\fuCert;
use wpm\furs\API\FURS;
use wpm\furs\Settings\FursSetting;
use wpm\furs\Traits\ModelManipulators;

class Premise extends Model
{
    use HasFactory, ModelHelpers, SoftDeletes;

    ###################################################################
    #                        INITIALIZATION                           #
    ###################################################################
    protected $fillable = [
        "intern_name",
        "TaxNumber",
        "BusinessPremiseID",
        "CadastralNumber",
        "BuildingNumber",
        "BuildingSectionNumber",
        "Street",
        "HouseNumber",
        "HouseNumberAdditional",
        "Community",
        "City",
        "PostalCode",
        "SpecialNotes",
        "PremiseType",
        "cert_used",
        "cert_password",
        "cert_path",
        "cert_name",
        "cert_verified",
        "reported_at",
        "closed",
        "business_unit_id",
    ];
    protected $with = [];
    protected $appends = [];
    protected $casts = [
        "TaxNumber" => "integer",
        "CadastralNumber" => "integer",
        "BuildingNumber" => "integer",
        "BuildingSectionNumber" => "integer",
    ];

    public const DEFAULT_SEARCH_COLS = [
        "intern_name",
        "TaxNumber",
    ];
    public const DEFAULT_PAGINATE = 30;


    ###################################################################
    #                           RELATIONS                             #
    ###################################################################
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function cash_registers()
    {
        return $this->hasMany(CashRegister::class);
    }

    public function businessUnit()
    {
        return $this->belongsTo(BusinessUnit::class);
    }

    ###################################################################
    #                           ACCESSORS                             #
    ###################################################################

    ###################################################################
    #                           MUTATORS                              #
    ###################################################################

    ###################################################################
    #                             SCOPES                              #
    ###################################################################
    public function scopeValid($query)
    {
        return $query->whereNotNull("reported_at");
    }

    ###################################################################
    #                           FUNCTIONS                             #
    ###################################################################

    /**
     * Constructs the business premise data for FURS reporting.
     *
     * Uses the fuCert class instance to extract necessary certificate details.
     *
     * @param fuCert $cert Certificate object including tax number and validity.
     * @return array Formatted data array suitable for FURS API submission.
     */
    public function fuBusinessPremise(fuCert $cert)
    {

        $data = [
            "TaxNumber" => intval($cert->taxNum),
            "BusinessPremiseID" => $this->BusinessPremiseID,
            "BPIdentifier" => $this->fuBPIdentifier(),
            "ValidityDate" => $cert->validityDate,
            "SoftwareSupplier" => [
                ["TaxNumber" => intval(config("furs.software_supplier_vat"))],
            ],
            "SpecialNotes" => $this->SpecialNotes ?? "Prijava poslovnega prostora!"
        ];

        if ($this->closed)
            $data["ClosingTag"] = "Z";

        return $data;
    }


    /**
     * Generates a business premise identifier based on the premise's type.
     * If a specific premise type is set, it uses that type directly.
     * Otherwise, it builds an identifier using the real estate details.
     *
     * @return array Business premise identifier as an associative array.
     */
    public function fuBPIdentifier()
    {
        return $this->PremiseType
            ? ["PremiseType" => $this->PremiseType]
            : [
                "RealEstateBP" => [
                    "PropertyID" => $this->fuPropertyID(),
                    "Address" => $this->fuAddress(),
                ]
            ];
    }


    /**
     * Creates the property ID data array for a real estate business premise.
     * This includes cadastral, building, and building section numbers,
     * all formatted as integers.
     *
     * @return array Property identification details.
     */
    public function fuPropertyID()
    {
        return array_map('intval', [
            "CadastralNumber" => $this->CadastralNumber,
            "BuildingNumber" => $this->BuildingNumber,
            "BuildingSectionNumber" => $this->BuildingSectionNumber,
        ]);
    }


    /**
     * Constructs the address data array for a business premise.
     * Includes essential address components such as street, house number, community, city, and postal code.
     * Conditionally adds house number additional if it is set.
     *
     * @return array Address details of the business premise.
     */
    public function fuAddress()
    {
        $data = [
            "Street" => $this->Street,
            "HouseNumber" => $this->HouseNumber,
            "Community" => $this->Community,
            "City" => $this->City,
            "PostalCode" => $this->PostalCode,
        ];

        if ($this->HouseNumberAdditional)
            $data["HouseNumberAdditional"] = $this->HouseNumberAdditional;

        return $data;
    }

    public function report()
    {
        $certificate = $this->getCertificate();
        $furs = new FURS($certificate);

        $response = $furs->fuPremise($this);
        $this->update(["reported_at" => Carbon::now()->format("Y-m-d H:i:s")]);
        return $response;
    }


    public function getCertificate()
    {
        if ($this->cert_used === "own") {
            $cert_path = $this->cert_path;
            $cert_password = $this->cert_password;
            if (!$this->cert_verified)
                throw new RuntimeException("Certifikat poslovnega prostora ni preverjen!");
        } else {
            $unit = $this->businessUnit;
            $cert_path = $unit->cert_path;
            $cert_password = $unit->cert_password;
            if (!$unit->cert_verified)
                throw new RuntimeException("Certifikat poslovne enote ni preverjen!");
        }

        $cert = new fuCert($cert_path, $cert_password);
        if ($cert->taxNum != $this->TaxNumber) {
            throw new \RuntimeException("Davčna številka certifikata (" . $cert->taxNum . ") ne ustreza davčni številki prostora. (" . $this->TaxNumber . ")", 400);
        }

        return $cert;
    }

}
