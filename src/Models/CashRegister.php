<?php

namespace wpm\furs\Models;

use d3x\starter\Traits\ModelHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use wpm\furs\Traits\PublishesMigrations;

class CashRegister extends Model
{
    use HasFactory, PublishesMigrations, ModelHelpers, SoftDeletes;

    ###################################################################
    #                        INITIALIZATION                           #
    ###################################################################
    protected $fillable = [
        "name",
        "intern_name",
        "number",
        "description",
        "business_unit_id",
    ];
    protected $with = [];
    protected $appends = [];

    public const DEFAULT_SEARCH_COLS = [
        "id",
        "name",
        "intern_name",
        "number",
    ];
    public const DEFAULT_PAGINATE = 30;

    ###################################################################
    #                           RELATIONS                             #
    ###################################################################

    ###################################################################
    #                           ACCESSORS                             #
    ###################################################################

    ###################################################################
    #                           MUTATORS                              #
    ###################################################################

    ###################################################################
    #                             SCOPES                              #
    ###################################################################

    ###################################################################
    #                           FUNCTIONS                             #
    ###################################################################

}
