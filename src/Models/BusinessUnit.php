<?php

namespace wpm\furs\Models;

use d3x\starter\Traits\ModelHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use wpm\furs\Traits\PublishesMigrations;

class BusinessUnit extends Model
{
    use HasFactory, ModelHelpers, SoftDeletes;

    ###################################################################
    #                        INITIALIZATION                           #
    ###################################################################
    protected $fillable = [
        "intern_name",
        "title",
        "TaxNumber",
        "CadastralNumber",
        "BuildingNumber",
        "BuildingSectionNumber",
        "Street",
        "HouseNumber",
        "HouseNumberAdditional",
        "Community",
        "City",
        "PostalCode",
        "cert_password",
        "cert_path",
        "cert_name",
        "cert_verified",
        "register_number",
        "email",
        "phone_number",
    ];
    protected $with = [];
    protected $appends = [];
    protected $casts = [
        "cert_verified" => "boolean",
        "TaxNumber" => "integer",
        "CadastralNumber" => "integer",
        "BuildingNumber" => "integer",
        "BuildingSectionNumber" => "integer",
    ];
    public const DEFAULT_SEARCH_COLS = [
        "intern_name",
        "title",
        "TaxNumber",
        "Street",
        "Community",
        "City",
    ];
    public const DEFAULT_PAGINATE = 30;

    ###################################################################
    #                           RELATIONS                             #
    ###################################################################

    ###################################################################
    #                           ACCESSORS                             #
    ###################################################################

    ###################################################################
    #                           MUTATORS                              #
    ###################################################################

    ###################################################################
    #                             SCOPES                              #
    ###################################################################

    ###################################################################
    #                           FUNCTIONS                             #
    ###################################################################

}
