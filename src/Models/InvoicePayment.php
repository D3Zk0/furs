<?php

namespace wpm\furs\Models;

use d3x\starter\Traits\ModelHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;
use wpm\furs\Traits\PublishesMigrations;

class InvoicePayment extends Model
{
    use HasFactory, ModelHelpers;

    ###################################################################
    #                        INITIALIZATION                           #
    ###################################################################
    protected $fillable = [
        "invoice_id",
        "payment_type_id",
        "value"
    ];

    public $timestamps = false; 

    protected $casts = [
        "value" => "float"
    ];

    protected $table = "invoice_payment";


    public const DEFAULT_SEARCH_COLS = [
    ];
    public const DEFAULT_PAGINATE = 30;

    ###################################################################
    #                           RELATIONS                             #
    ###################################################################

    public function payment_type()
    {
        return $this->belongsTo(PaymentType::class, 'payment_type_id');
    }

    ###################################################################
    #                           ACCESSORS                             #
    ###################################################################

    ###################################################################
    #                           MUTATORS                              #
    ###################################################################

    ###################################################################
    #                             SCOPES                              #
    ###################################################################

    ###################################################################
    #                           FUNCTIONS                             #
    ###################################################################

}
