<?php

namespace wpm\furs;

use d3x\starter\StarterServiceProvider;
use Illuminate\Support\ServiceProvider;
use wpm\furs\Components\Customer;
use wpm\furs\Components\ForPayment;
use wpm\furs\Components\Identifiers;
use wpm\furs\Components\InvoiceHead;
use wpm\furs\Components\InvoiceItems;
use wpm\furs\Components\QR;
use wpm\furs\Settings\FursSetting;
use wpm\furs\Traits\PublishesMigrations;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Log;
use \wpm\furs\Commands\CheckForFailedInvoiceReports;

class FursServiceProvider extends ServiceProvider
{
    use PublishesMigrations;

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                CheckForFailedInvoiceReports::class,
            ]);
        }

        $this->registerMigrations(__DIR__ . '/database/migrations');
//        $this->loadRoutesFrom(__DIR__ . '/routes/api.php');
//        $this->loadRoutesFrom(__DIR__ . '/routes/web.php');
        // Define the path for published and package views
        $packageViewsPath = __DIR__ . '/resources/views';
        $publishedViewsPath = resource_path('views/vendor/furs');

        // Load the published views first, then fallback to package views
        $this->loadViewsFrom([$publishedViewsPath, $packageViewsPath], 'furs');

        // Publish only the "pdf" folder from the package views
        $this->publishes([
            $packageViewsPath . '/pdf' => $publishedViewsPath . '/pdf',
        ], 'furs-views');


        $this->publishes([
            __DIR__ . './routes/api.php' => base_path('routes/furs/furs-api.php'),
            __DIR__ . './routes/web.php' => base_path('routes/furs/furs-web.php'),
        ], 'furs-routes');

        $this->publishes([
            __DIR__ . "/../dist" => base_path('public/furs-app'),
        ], 'furs-app');

        Blade::component("invoice-items", InvoiceItems::class);
        Blade::component("for-payment", ForPayment::class);
        Blade::component("qr", QR::class);
        Blade::component("identifiers", Identifiers::class);
        Blade::component("invoice-head", InvoiceHead::class);
        Blade::component("customer", Customer::class);

        $this->publishes([
            __DIR__ . '/database/settings/' => database_path('settings/'),
        ], 'furs-settings');
        $this->publishes([
            __DIR__ . '/database/migrations/' => database_path('migrations/'),
        ], 'furs-migrations');

        $this->publishes([
            __DIR__ . '/config/furs.php' => config_path('furs.php'),
        ], "furs-config");
    }


    public function register()
    {
        $this->app->register(StarterServiceProvider::class);
        $this->mergeConfigFrom(
            __DIR__ . '/config/furs.php',
            'furs'
        );
        $this->loadHelpers();
    }

    /**
     * Load the helpers file.
     *
     * @return void
     */
    protected function loadHelpers()
    {
        $file = __DIR__ . '/../helpers.php';
        if (file_exists($file)) {
            require $file;
        }

        $file = __DIR__ . '/../format_helpers.php';
        if (file_exists($file)) {
            require $file;
        }
    }
}
