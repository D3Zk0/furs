<?php

namespace wpm\furs\API;

use Exception;

class fuCert
{
    // CERT
    protected $name;
    protected $password;
    protected $path;
    protected $key;
    public $validityDate;

    // CERTINFO
    protected $alg;
    protected $serial;
    protected $issuer;
    protected $subject;
    public $taxNum;

    public function __construct($path, $password)
    {
        $this->path = $path;
        $this->password = $password;
        $this->init();
    }

    private function init()
    {
        $cert = [];
        if (!file_exists($this->path)) {
            throw new Exception("The certificate file does not exist at path: {$this->path}");
        }

        $pfxContent = file_get_contents($this->path);
        if (!$pfxContent) {
            throw new Exception("Unable to read the certificate file at path: {$this->path}");
        }

        if (!openssl_pkcs12_read($pfxContent, $cert, $this->password)) {
            dd($cert, $this->password);
            throw new Exception("Unable to read the PKCS12 certificate. Please check the password and the file.");
        }

        $data = openssl_x509_parse($cert['cert'], 0);
        if ($data === false) {
            throw new Exception("Unable to parse the X.509 certificate.");
        }

        $this->key = $cert['pkey'];

        // Defining issuer
        $issuer = $data["issuer"];
        $this->issuer = "CN=" . $issuer["commonName"] . ",O=" . $issuer["organizationName"] . ",C=" . $issuer["countryName"];

        // Defining subject
        $subject = $data["subject"];
        $this->taxNum = $subject["organizationalUnitName"][1];
        $this->subject = "CN=" . $subject["commonName"] . ",OU=" . $subject["organizationalUnitName"][1] . ",OU=" . $subject["organizationalUnitName"][0] . ",O=" . $subject["organizationName"] . ",C=" . $subject["countryName"];
        $this->serial = $data["serialNumber"];
        $this->alg = "RS256"; #$data["signatureTypeSN"]; //možnost napake
        $this->validityDate = date('Y-m-d', $data['validTo_time_t']);
    }

    public function getTaxNumber()
    {
        return (int)$this->taxNum;
    }

    public function getHeader()
    {
        return [
            "alg" => $this->alg,
            "subject_name" => $this->subject,
            "issuer_name" => $this->issuer,
            "serial" => $this->serial,
        ];
    }

    public function getPemCert()
    {
        $pemPath = str_replace(".p12", ".pem", $this->path);
        if (!file_exists($pemPath)) {
            $cert = [];
            if (openssl_pkcs12_read(file_get_contents($this->path), $cert, $this->password)) {
                file_put_contents($pemPath, $cert['cert']);
            } else {
                throw new Exception("Unable to convert the certificate to PEM format.");
            }
        }
        return $pemPath;
    }

    public function getKey()
    {
        return $this->key;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getPassword()
    {
        return $this->password;
    }
}
