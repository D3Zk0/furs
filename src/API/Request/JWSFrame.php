<?php

namespace wpm\furs\API\Request;
use Carbon\Carbon;
use Exception;
use Gamegos\JWS\JWS;

abstract class JWSFrame
{
    protected $header;
    protected $signature;
    public $payload;
    protected $message;
    protected $cert;


    //FUNCTION SETS THE PAYLOAD OF REQUEST :NONE
    public function setJWSPayload($payload)
    {
        $this->payload = $payload;
    }

    //FUNCTION RETURNS JWT :STRING
    public function getToken()
    {
        try {
            $this->header = $this->cert->getHeader();
            if (!$this->header || !$this->payload)
                throw new Exception();
            return (new JWS())->encode($this->header, $this->payload, $this->cert->getKey());
        } catch (Exception $e) {
            return $e;
        }
    }

    //FUNCTION RETURNS CURRENT DATETIME :DATETIME (Y-m-dTH:i:s)
    public function getDateTime()
    {
        return Carbon::now()->format("Y-m-d") . "T" . Carbon::now()->format("H:i:s");
    }

    //FUNCTION RETURNS FU JWT in JSON FORMAT :STRING(JSON)
    public function jsonFormat()
    {
        return json_encode(["token" => $this->getToken()], JSON_THROW_ON_ERROR);
    }

    //FUNCTION RETURNS HEADER :ARRAY
    public function getHeader()
    {
        return [
            "MessageID" => $this->returnUUID(),
            "DateTime" => $this->getDateTime()
        ];
    }

    //FUNCTION RETURNS UUID :STRING
    protected function returnUUID()
    {
        // in case of PHP 7 use random_bytes
        $data = random_bytes(16);
        # $data = openssl_random_pseudo_bytes(16);
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }
}
