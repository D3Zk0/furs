<?php

namespace wpm\furs\API\Request;

use wpm\furs\API\fuCert;
use wpm\furs\Models\Premise;

class fuPremise extends JWSFrame
{
    protected $premise;

    public function __construct(fuCert $cert, Premise $premise = null)
    {
        $this->cert = $cert;
        $this->premise = $premise;
        if ($this->premise)
            $this->setBPdata();
        else
            $this->createTestBusinessPremiseMessage();
    }

    //FUNCTION CREATE REAL DATA FOR fuBusinessPremise :NONE
    public function setBPdata()
    {
        $payload = [
            "BusinessPremiseRequest" => [
                "Header" => $this->getHeader(),
                "BusinessPremise" => $this->premise->fuBusinessPremise($this->cert),
            ]
        ];
        $this->setJWSPayload($payload);
    }

    private function createTestBusinessPremiseMessage()
    {
        $payload = [
            "BusinessPremiseRequest" => [
                "Header" => $this->getHeader(),
                "BusinessPremise" => [
                    "TaxNumber" => "dejmo tel neki druzga",
                    "BusinessPremiseID" => "36CF",
                    "BPIdentifier" => [
                        "RealEstateBP" => [
                            "PropertyID" => [
                                "CadastralNumber" => 365,
                                "BuildingNumber" => 12,
                                "BuildingSectionNumber" => 3
                            ],
                            "Address" => [
                                "Street" => "Tržaška cesta",
                                "HouseNumber" => "24",
                                "HouseNumberAdditional" => "B",
                                "Community" => "Ljubljana",
                                "City" => "Ljubljana",
                                "PostalCode" => "1000"
                            ]
                        ]
                    ],
                    "ValidityDate" => $this->getDateTime(),
                    "SoftwareSupplier" => [
                        [
                            "TaxNumber" => 24564444,
                        ]
                    ],
                    "SpecialNotes" => "Primer prijave poslovnega prostora"
                ]
            ]
        ];
        $this->setJWSPayload($payload);
    }


}
