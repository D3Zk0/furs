<?php


namespace wpm\furs\API\Request;


use Gamegos\JWS\JWS;
use wpm\furs\API\fuCert;

/**
 * The fuEcho class extends JWSFrame to handle echo requests.
 * This class is specifically designed to encapsulate an echo request message as part of JSON Web Signature (JWS) operations,
 * typically used to verify connectivity or to echo back messages from the server for testing or diagnostic purposes.
 *
 * Usage:
 * $msg = new fuEcho("test");  // Instantiates an echo request with the message "test"
 *
 * The constructor initializes the object with a message that will be part of the EchoRequest sent to the server.
 */
class fuEcho extends JWSFrame
{
    /**
     * Constructor for fuEcho class. Initializes an echo request with a specific message.
     *
     * @param string $message The message to be sent as an echo request. This message will be wrapped inside an EchoRequest array.
     */
    public function __construct(fuCert $cert, $message)
    {
        $this->message = [
            "EchoRequest" => $message
        ];
    }

    public function jsonFormat()
    {
        return json_encode($this->getToken(), JSON_THROW_ON_ERROR);
    }


    public function getToken()
    {
        return $this->message;
    }

}
