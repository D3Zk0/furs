<?php

namespace wpm\furs\API\Request;

use wpm\furs\API\fuCert;
use wpm\furs\Models\Invoice;
use wpm\furs\Models\Premise;

class fuInvoice extends JWSFrame
{
    protected $premise;

    public function __construct(fuCert $cert, Invoice $invoice)
    {
        $this->cert = $cert;
        $this->premise = $invoice->premise()->first();
        $this->cash_register = $invoice->cash_register()->first();
        $this->setInvoiceData($invoice);
    }

    private function setInvoiceData(Invoice $invoice)
    {
        $data = [
            "InvoiceRequest" => [
                "Header" => $this->generateHeader(),
                "Invoice" => $this->generateInvoiceData($invoice),
            ],
        ];

        if ($relatedData = $invoice->getReferenceInvoiceData()) {
            $data["InvoiceRequest"]["Invoice"]["ReferenceInvoice"] = $relatedData;
        }

        if ($invoice->CustomerVATNumber) {
            $data["InvoiceRequest"]["Invoice"]["CustomerVATNumber"] = $invoice->CustomerVATNumber;
        }

        $this->setJWSPayload($data);
    }

    private function generateHeader()
    {
        return [
            "MessageID" => $this->returnUUID(),
            "DateTime" => $this->getDateTime(),
        ];
    }

    private function generateInvoiceData(Invoice $invoice)
    {
        $data = [
            "TaxNumber" => $this->cert->getTaxNumber(),
            "IssueDateTime" => $invoice->issueDateTime(),
            "NumberingStructure" => "B", // TODO: add choice between C and B (by premise or cash register)
            "InvoiceIdentifier" => $invoice->invoiceIdentifier(),
            "InvoiceAmount" => round($invoice->InvoiceAmount, 2),
            "PaymentAmount" => round($invoice->PaymentAmount, 2),
            "TaxesPerSeller" => $invoice->taxesPerSeller(),
            "OperatorTaxNumber" => (int)$invoice->OperatorTaxNumber ?: (int)$this->cert->getTaxNumber(),
            "ProtectedID" => $invoice->ZOI,
        ];
        if ($invoice->reported_at == null and $invoice->report_status == "failed") {
            $data["SubsequentSubmit"] = true;
        }
        return $data;
    }


}
