<?php

namespace wpm\furs\API\Response;

use wpm\furs\API\Exceptions\FURSException;

abstract class fuResponse
{
    protected $status;
    protected $errors;
    protected $message;
    protected $header;
    protected $payload;

    public function __construct($response)
    {
        $this->header = $response["headers"];
        $this->payload = $response["payload"];
        $this->init();
    }

    public function init()
    {
        $this->checkForError();
    }


    public function checkForError()
    {
        $error = self::findError($this->payload);

        if ($error) {
            $message = $error["ErrorMessage"];
            $code = $error["ErrorCode"];
            throw new FURSException($code, $message);
        }
    }

    public static function findError($data)
    {
        $error = null;
        foreach ($data as $key => $value) {
            if ($key === "Error")
                return $value;

            if (is_array($value) || is_object($value)){
                $error = self::findError($value);
            }
        }
        return $error;
    }

}
