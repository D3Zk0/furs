<?php

namespace wpm\furs\API\Response;

class fuInvoiceResponse extends fuResponse
{

    public function getUniqueInvoiceID()
    {
        return $this->UniqueInvoiceID;
    }


    public function getEOR()
    {
        return $this->payload["InvoiceResponse"]["UniqueInvoiceID"];
    }

    public function getDateTime()
    {
        return $this->payload["InvoiceResponse"]["Header"]["DateTime"];
    }


}
