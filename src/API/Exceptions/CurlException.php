<?php

namespace wpm\furs\API\Exceptions;

use Exception;

class CurlException extends Exception
{
    public function __construct($code, $message)
    {
        $msg =  "CURL: ".$message." [".$code."]";
        parent::__construct($msg, 500);
    }
}
