<?php

namespace wpm\furs\API\Exceptions;

use Exception;
use Throwable;

class SignatureVerificationException extends Exception
{
    /**
     * Constructor for the SignatureVerificationException.
     * Allows for a custom message and optionally a custom code.
     *
     * @param string $message Error message for the exception.
     * @param int $code Optional error code (defaults to 0).
     * @param Throwable|null $previous Optional previous exception for exception chaining.
     */
    public function __construct($message, $code = 0, Throwable $previous = null)
    {
        // Ensure all parameters are passed to the parent constructor
        parent::__construct($message, $code, $previous);
    }

    /**
     * Custom string representation of the exception.
     * Useful for logging or displaying a more informative error message.
     *
     * @return string Custom string describing the exception.
     */
    public function __toString()
    {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }
}
