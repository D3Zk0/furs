<?php

namespace wpm\furs\API\Exceptions;
use Exception;

class FURSException extends Exception
{
    public function __construct($code, $message)
    {
        $msg =  "FURS: ". $message." [".$code."]";
        parent::__construct($msg, 500);
    }

}
