<?php

namespace wpm\furs\API;


use Exception;
use Gamegos\JWS\JWS;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use wpm\furs\API\Exceptions\CurlException;
use wpm\furs\API\Exceptions\SignatureVerificationException;
use wpm\furs\API\Request\fuEcho;
use wpm\furs\API\Request\fuInvoice;
use wpm\furs\API\Request\fuPremise;
use wpm\furs\API\Response\fuInvoiceResponse;
use wpm\furs\API\Response\fuPremiseResponse;
use wpm\furs\Models\Invoice;
use wpm\furs\Models\Premise;
use wpm\furs\Settings\FursSetting;
use wpm\furs\Utils\QR;

class FURS
{
    /*
     * DOCUMENTATION LINK:
     * https://edavki.durs.si/EdavkiPortal/OpenPortal/CommonPages/Opdynp/PageD.aspx?category=dpr_teh_spec
     * */
    public $cert;
    protected $url;
    protected $SIGcert;
    protected $doc;
    protected $response;
    protected $action;
    protected $header = [
        'Content-Type: application/json; charset=utf-8',
    ];

    public const ROOT_CERT = [
        "pem" => "root_cert.pem",
        "crt" => "root_cert.crt",
        "pub" => "root_cert.pub",
    ];

    /**
     * Constructor for initializing the system in either production or test mode.
     * Sets the appropriate environment based on the provided flag.
     *
     * @param string $mode Set to true to initialize in production mode; otherwise, in test mode.
     */
    public function __construct(fuCert $cert)
    {
        $this->cert = $cert;
        $settings = app(FursSetting::class);
        if ($settings->mode === "prod") {
            $this->setProductionMode();
        } else {
            $this->setTestMode();
        }
    }

    /**
     * Initializes the system for operation in a production environment.
     * Sets the base URL and paths to production certificates used for SSL/TLS and signature verification.
     */
    public function setProductionMode()
    {
        $this->url = "https://blagajne.fu.gov.si:9003/v1/cash_registers";
        $this->SIGcert = cert_path("SIG.pem");
    }

    /**
     * Initializes the system for operation in a test environment.
     * Sets the base URL and paths to test certificates used for SSL/TLS and signature verification.
     */
    public function setTestMode()
    {
        $this->url = "https://blagajne-test.fu.gov.si:9002/v1/cash_registers"; // Test API endpoint
        $this->SIGcert = cert_path("test/SIG.pem");
    }


    ###################################################################
    #                           REQUESTS                              #
    ###################################################################

    /**
     * Sends an echo request to the FURS API endpoint to test connectivity or message passing.
     * This function sets the action to '/echo' and initializes an echo document with a provided message.
     * It then submits this message to the FURS system using the post2Furs method, facilitating API testing or debugging.
     *
     * @param string $message The message to be echoed back by the API. Defaults to "test" if no message is provided.
     * @return mixed The response from the FURS system after submitting the echo request, typically the echoed message.
     * @throws Exception If the premise associated with the invoice is not set.
     */
    public function fuEcho(fuCert $cert, string $message = "test")
    {
        $this->cert = $cert;
        $this->action = "/echo";
        $this->url .= $this->action;
        $this->doc = new fuEcho($message);

        return $this->post2Furs();
    }


    /**
     * Submits an invoice to the FURS system for fiscal verification and updates the invoice data.
     * If the premise associated with the invoice is not found, an exception is thrown.
     * After submission, updates the invoice with relevant fiscal data including EOR, ZOI, and QR code.
     *
     * @param Invoice $invoice The invoice object to be processed and reported.
     * @throws Exception If the premise associated with the invoice is not set.
     */
    public function fuInvoice(Invoice $invoice): void
    {
        $this->action = "/invoices";
        $this->url .= $this->action;

        // Initialize certification and document for submission.
        $this->doc = new fuInvoice($this->cert, $invoice);
        $response = $this->post2Furs();

        // Extract fiscal data from the response.
        $EOR = $response->getEOR();
        $reported_at = $response->getDateTime();

        // Update the invoice with the fiscal data.
        $invoice->update([
            "EOR" => $EOR,
            "reported_at" => $reported_at,
            "report_status" => "successful",
        ]);

        // Generate QR code and update the invoice.
        $QR = $this->generateQR($invoice);
        $invoice->update(["QR" => $QR]);
    }

    /**
     * Registers a premise with the FURS system using the '/invoices/register' API endpoint.
     * Constructs a new fuCert object with the premise's certificate and submits the registration
     * using a fuPremise document.
     *
     * @param Premise|null $premise The premise to be registered. Defaults to null to allow flexible calling.
     * @return mixed The response from the FURS system after submitting the registration.
     * @throws Exception If premise data is missing or incorrect.
     */
    public function fuPremise(Premise $premise = null)
    {
        // Define the API action for premise registration.
        $this->action = "/invoices/register";
        $this->url .= $this->action;

        // Setup certificate and document for the premise.
        if (!$premise) {
            throw new \RuntimeException("Premise data is required for registration.");
        }
        $this->doc = new fuPremise($this->cert, $premise);
        // Submit the registration and return the response.
        return $this->post2Furs();
    }


    /**
     * Submits a test invoice check request to the FURS system.
     * This function is typically used in a testing or development environment
     * to validate the connectivity and response handling of the system without affecting live data.
     * The function sets the system to test mode and submits a check request.
     *
     * @return mixed The response from the FURS system after submitting the check request.
     * @throws Exception If the premise associated with the invoice is not set.
     */
    public function fuInvoiceCheck()
    {
        $this->setTestMode();
        return $this->post2Furs();
    }

    /**
     * Submits a test business premise check request to the FURS system.
     * Similar to fuInvoiceCheck, this function is used to confirm system readiness and API response
     * in a non-production environment. It sets the application to test mode and submits the request.
     *
     * @return mixed The response from the FURS system after submitting the premise check request.
     * @throws Exception If the premise associated with the invoice is not set.
     */
    public function fuBusinessPremiseCheck()
    {
        $this->setTestMode();
        return $this->post2Furs();
    }


    /**
     * Sends a JSON formatted document to the FURS API using a POST request.
     * Configures cURL with necessary options including timeouts, SSL certificates, and custom headers.
     * Throws an exception if the cURL operation fails.
     *
     * @return mixed The API response token or throws an exception on failure.
     * @throws CurlException If the cURL request fails.
     * @throws SignatureVerificationException
     * @throws \JsonException
     * @throws Exception
     */
    public function post2Furs()
    {
        //        $caCertPinnedPub = cert_path(self::ROOT_CERT["pub"]);

        //        $pinnedPubKey = "vUYCPT8eosYkqW1RkQGCqzE0TtpdLay6EQCOEP9MqJ4=";
        //openssl dgst -sha256 -binary "C:\Users\d3x\Documents\Work\Laravel\Packages\furs\certificates\root_cert.pub" | openssl base64
        //        $pinnedPubKey = trim($pinnedPubKey);

        $config = [
            CURLOPT_URL => $this->url,
            CURLOPT_FRESH_CONNECT => true,
            CURLOPT_CONNECTTIMEOUT_MS => 3000,
            CURLOPT_TIMEOUT_MS => 3000,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => $this->header,
            CURLOPT_POSTFIELDS => $this->doc->jsonFormat(),
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSLCERT => $this->cert->getPemCert(),
            CURLOPT_SSLCERTPASSWD => $this->cert->getPassword(),
            CURLOPT_CAINFO => cert_path(self::ROOT_CERT['pem']),
        ];


        $conn = curl_init();
        curl_setopt_array($conn, $config);
        $response = curl_exec($conn);
        if ($response === false) {
            throw new CurlException(curl_errno($conn), curl_error($conn));
        }


        $decoded_response = json_decode($response, false, 512, JSON_THROW_ON_ERROR);


        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new \RuntimeException('Failed to decode JSON response: ' . json_last_error_msg());
        }

        if (!is_object($decoded_response)) {
            // Debug the response if it's not an object
            throw new \RuntimeException('An error occurred or the service is unavailable. Response:' . $response);
        }

        if (property_exists($decoded_response, 'token')) {
            $token = $decoded_response->token;
            return $this->handleResponse($token);
        }

        return $decoded_response;
    }

    /**
     * Processes a token response from the API, verifying its signature and returning
     * the appropriate response object based on the predefined action.
     *
     * Uses OpenSSL to verify the signature of the token using the public key from the SIGcert.
     * Depending on the action, it returns different response types or the raw response.
     *
     * @param string $token The token received from the API.
     * @return mixed Depending on the action, returns an instance of fuInvoiceResponse,
     *               fuPremiseResponse, or the raw response object.
     * @throws SignatureVerificationException If signature verification fails.
     */
    public function handleResponse($token)
    {
        // Load the public key from a certificate file to verify the token's signature
        $key = openssl_pkey_get_public(file_get_contents($this->SIGcert));
        $verified = (new JWS())->verify($token, $key);

        if (!$verified) {
            throw new SignatureVerificationException("Failed to verify the token's signature.");
        }

        // Determine the type of response to return based on the action
        switch ($this->action) {
            case "/invoices":
                return new fuInvoiceResponse($verified);
            case "/invoices/register":
                return new fuPremiseResponse($verified);
            default:
                return $verified;
        }
    }

    /**
     * Generates a QR code for an invoice that encodes the ZOI in decimal format, tax number,
     * and date/time of the invoice issue. The QR code also includes a control character for validation.
     *
     * @param Invoice $invoice The invoice object for which the QR code is generated.
     * @return string Base64-encoded representation of the QR code.
     */
    public function generateQR(Invoice $invoice): string
    {
        // Convert ZOI to decimal and pad with leading zeros to match required length.
        $zoiDecimal = QR::getZOIDecimal($invoice->ZOI);
        $zoiDecimal = str_pad($zoiDecimal, 39, '0', STR_PAD_LEFT);

        // Extract and format the date and time from the invoice for the QR code.
        $dateTime = str_replace(['-', 'T', ':'], '', $invoice->issueDateTime());
        $dateTime = substr($dateTime, 2); // Remove the first two digits of the year.

        // Combine elements to form the QR code base string.
        $qrBase = $zoiDecimal . $this->cert->getTaxNumber() . $dateTime;

        // Calculate the control character.
        $controlChar = array_sum(str_split($qrBase)) % 10;

        // Append control character and generate the QR code.
        $qrCode = QRcode::generate($qrBase . $controlChar);

        // Return the QR code in Base64 format to ensure it can be easily transmitted and displayed.
        return base64_encode($qrCode);
    }
}
