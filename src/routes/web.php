<?php

use Illuminate\Support\Facades\Route;
use wpm\furs\Http\Controllers\{
    FailedInvoiceController,
    HomeController,
};

# ==> furs <== #
Route::group(['prefix' => 'furs'], function () {
    Route::get("/logs/{id}", [FailedInvoiceController::class, "logs"]);
    Route::get("/{any?}", [HomeController::class, "index"])->where('any', '.*');
});
