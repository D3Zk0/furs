<?php

use Illuminate\Support\Facades\Route;
use wpm\furs\Http\Controllers\{
    CashRegisterController,
    CustomerController,
    OperatorController,
    FailedInvoiceController,
    PaymentTypeController,
    InvoiceController,
    BusinessUnitController,
    CertificateController,
    PremiseController,
    SettingsController
};

Route::group(["prefix" => "furs"], function () {
    # ==> api <== #
    Route::group(['prefix' => 'api'], function () {

        # ==> settings <== #
        Route::group(['prefix' => 'settings'], function () {
            #------------------- DEFAULT --------------------#
            Route::get("/get-mode", [SettingsController::class, "getMode"]);
            Route::post('/switch-mode', [SettingsController::class, "switchMode"]);

            #------------------- CUSTOM ---------------------#
            Route::get('/load', [SettingsController::class, "loadSettings"]);
            Route::post('/store-info', [SettingsController::class, "storeInfo"]);
        });


        # ==> business-unit <== #
        Route::group(['prefix' => 'business-unit'], function () {
            #------------------- DEFAULT --------------------#
            Route::post('/paginate', [BusinessUnitController::class, "paginate"]);
            Route::get('/show/{id}', [BusinessUnitController::class, "show"]);
            Route::post('/store', [BusinessUnitController::class, "store"]);
            Route::delete('/delete/{id}', [BusinessUnitController::class, "delete"]);

            #------------------- CUSTOM ---------------------#
        });

        # ==> premise <== #
        Route::group(['prefix' => 'premise'], function () {
            #------------------- DEFAULT --------------------#
            Route::post('/paginate', [PremiseController::class, "paginate"]);
            Route::get('/show/{id}', [PremiseController::class, "show"]);
            Route::post('/store', [PremiseController::class, "store"]);
            Route::delete('/delete/{id}', [PremiseController::class, "delete"]);

            #------------------- CUSTOM ---------------------#
            Route::post('/report/{id}', [PremiseController::class, "report"]);
        });

        # ==> failed-invoice <== #
        Route::group(['prefix' => 'failed-invoice'], function () {
            #------------------- DEFAULT --------------------#
            Route::post('/paginate', [FailedInvoiceController::class, "paginate"]);

            #------------------- CUSTOM ---------------------#
        });

        # ==> register <== #
        Route::group(['prefix' => 'register'], function () {
            #------------------- DEFAULT --------------------#
            Route::post('/paginate', [CashRegisterController::class, "paginate"]);
            Route::get('/show/{id}', [CashRegisterController::class, "show"]);
            Route::post('/store', [CashRegisterController::class, "store"]);
            Route::delete('/delete/{id}', [CashRegisterController::class, "delete"]);

            #------------------- CUSTOM ---------------------#
        });

        # ==> invoice <== #
        Route::group(['prefix' => 'invoice'], function () {
            #------------------- DEFAULT --------------------#
            Route::post('/paginate', [InvoiceController::class, "paginate"]);
            Route::get('/show/{id}', [InvoiceController::class, "show"]);
            Route::post('/store', [InvoiceController::class, "store"]);
            Route::delete('/delete/{id}', [InvoiceController::class, "delete"]);

            #------------------- CUSTOM ---------------------#
            Route::get("/{id}/pdf", [InvoiceController::class, "downloadInvoicePdf"]);
        });

        # ==> customer <== #
        Route::group(['prefix' => 'customer'], function () {
            #------------------- DEFAULT --------------------#
            Route::post('/paginate', [CustomerController::class, "paginate"]);
            Route::get('/show/{id}', [CustomerController::class, "show"]);
            Route::post('/store', [CustomerController::class, "store"]);
            Route::delete('/delete/{id}', [CustomerController::class, "delete"]);

            #------------------- CUSTOM ---------------------#
        });

        # ==> operator <== #
        Route::group(['prefix' => 'operator'], function () {
            #------------------- DEFAULT --------------------#
            Route::post('/paginate', [OperatorController::class, "paginate"]);
            Route::get('/show/{id}', [OperatorController::class, "show"]);
            Route::post('/store', [OperatorController::class, "store"]);
            Route::delete('/delete/{id}', [OperatorController::class, "delete"]);
        });

        # ==> payment-type <== #
        Route::group(['prefix' => 'payment-type'], function () {
            #------------------- DEFAULT --------------------#
            Route::post('/paginate', [PaymentTypeController::class, "paginate"]);
            Route::get('/show/{id}', [PaymentTypeController::class, "show"]);
            Route::post('/store', [PaymentTypeController::class, "store"]);
            Route::delete('/delete/{id}', [PaymentTypeController::class, "delete"]);
        });

        # ==> certificate <== #
        Route::group(['prefix' => 'certificate'], function () {
            #------------------- CUSTOM ---------------------#
            Route::get("/download", [CertificateController::class, "download"]);
            Route::post("/verify", [CertificateController::class, "verify"]);
        });


    });

});